<?php /*
THE CONTAINER FOR SEARCH RESULTS PAGES
*/ ?>

<?php get_header(); ?>

<main class="full-width">
  <!-- PAGE TOP / PAGE TITLE / BANNER / SLIDESHOW / ETC -->
  <?php get_template_part( 'template-parts/content', 'page-top' ); ?>
	<section class="search-feed max-width certificates-container">

		<!-- LOOP START -->
		<?php if ( have_posts() ) : ?>
		<?php while ( have_posts() ) : the_post();

			/* SEARCH RESULT TEMPLATE */
			get_template_part( 'template-parts/content', 'search' );

		endwhile; ?>

		<!-- NAVIGATION -->
		<div style="clear: both"></div>
		<?php the_posts_pagination( array(
			'prev_text'          => __( 'Previous page', 'twentysixteen' ),
			'next_text'          => __( 'Next page', 'twentysixteen' ),
			'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>',
		) );

		else :

      /* TEMPLATE FOR NO RESULTS */
      get_template_part( 'template-parts/content', 'none' );

    endif; ?>
    <!-- LOOP END -->

	</section>
</main>

<?php get_footer(); ?>