<?php /*
THE TEMPLATE FOR DISPLAYING ARCHIVES & CATEGORY FOR BLOG
*/ ?>

<?php get_header(); ?>

<main class="full-width">

  <!-- PAGE TOP / PAGE TITLE / BANNER / SLIDESHOW / ETC -->
  <?php get_template_part( 'template-parts/content', 'page-top' ); ?>

  <!-- BLOG POST LOOP-->
  <section class="blog-feed post-feed">
    <!--FEATURED POST-->
    <?php get_template_part( 'template-parts/content', 'post-preview-featured' ); ?>
    <div class="certificates-container max-width">
      <?php echo do_shortcode('[ajax_load_more container_type="div" post_type="post" posts_per_page="4" meta_key="featured" meta_value="Yes" meta_compare="!=" scroll="false" button_label="Load More"]'); ?>
    </div>
  </section>
  
</main>

<?php get_footer(); ?>