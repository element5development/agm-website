<?php /*
DEFAULT PAGE TEMPLATE
*/ ?>

<?php get_header(); ?>

<main class="full-width">

  <!-- PAGE TOP / PAGE TITLE / BANNER / SLIDESHOW / ETC -->
  <?php get_template_part( 'template-parts/content', 'page-top' ); ?>

<?php if( is_page('contact') ) {
  //nothing
} else {
  if ( have_rows('navigation') ) {
    get_template_part( 'template-parts/content', 'anchor-navigation' );
  } 
}?>
	<!-- ADD PAGE CONTENT -->
<?php if( is_page('careers') || is_page('products') || is_page('engineering') || is_page('capabilities')){ ?>
    
    <div class="page-contents full-width">
    <a id="1" class='anchor'></a>
    <?php if (have_posts()) : ?>
      <?php while (have_posts()) : the_post(); ?>
        <?php the_content(); ?>
      <?php endwhile; ?>
    <?php endif; ?>
  </div>
<?php } elseif(!is_page('contact')) { ?>
  
	<div class="page-contents max-width">
  <a id="1" class='anchor'></a>
		<?php if (have_posts()) : ?>
			<?php while (have_posts()) : the_post(); ?>
				<?php the_content(); ?>
			<?php endwhile; ?>
		<?php endif; ?>
	</div>
<?php } else { ?>
    <div class="page-contents max-width">
    <?php if (have_posts()) : ?>
      <?php while (have_posts()) : the_post(); ?>
        <?php the_content(); ?>
      <?php endwhile; ?>
    <?php endif; ?>
    </div>
<?php }?>
	<!-- ADD PAGE CONTENT -->

  <!-- QUALITY PAGE AWARDS SLIDER -->
  <?php if(is_page('quality') || is_page('about') ){
    get_template_part('template-parts/content','awards-slider');
  }?>

  <!-- ENGINEERING PAGE CORE COMPETENCIES -->
  <?php if(is_page('engineering') || is_page('capabilities') || is_page('quality') ){
    get_template_part('template-parts/content','benefits');
    } ?>

  <!-- PRODUCT CATEGORIES / PRODUCTS / PRODUCT DETAILS -->
  <?php if ( is_page( 1617 ) ) { //PRODUCT PAGE
    get_template_part( 'template-parts/content', 'products' );
  } ?>

  <!--GLOBAL FOOTPRINT/ABOUT PAGE -->
  <?php if ( is_page('about') ) {
    get_template_part('template-parts/content', 'global-footprint');
  } ?>
  <?php if ( is_page('about') ) {
    get_template_part('template-parts/content', 'originativity');
  } ?>
  <!-- LEADERSHIP SHORT BIOS / TEAM MEMBER PHOTOS -->
  <?php if ( is_page('about') ) {
    get_template_part( 'template-parts/content', 'leadership' );
  } ?>

  <!-- CERTIFICATES -->
  <?php if ( have_rows('certifications') ) {
    get_template_part( 'template-parts/content', 'certificates' );
  } ?>

  <!-- LOCATION CONTACT INFO -->
  <?php if ( have_rows('na_locations') || have_rows('europe_locations')  || have_rows('asia_locations') ) {
      if ( have_rows('navigation') ) {
        get_template_part( 'template-parts/content', 'anchor-navigation' );
        } 
    get_template_part( 'template-parts/content', 'location-contact' );
  } ?>

  <!-- CARD INFORMATION -->
  <?php if ( have_rows('cards') ) {
    if(is_page('engineering')){
    get_template_part( 'template-parts/content', 'design-dev' );
    } else {
    get_template_part( 'template-parts/content', 'page-cards' );
    }
  } ?>

  <!-- CAREERS -->
  <?php if ( is_page ( 1615 ) ) { //CAREERS PAGE
    get_template_part('template-parts/content','benefits');
    get_template_part('template-parts/content','join-team');
  } ?>


</main>

<?php get_footer(); ?>