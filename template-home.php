<?php /*
Template Name: Home Page
*/ ?>

<?php get_header(); ?>

<main class="full-width">

  <!-- PAGE TOP / PAGE TITLE / BANNER / SLIDESHOW / ETC -->
  <?php get_template_part( 'template-parts/content', 'page-top' ); ?>

  <!-- ADD PAGE CONTENT -->
  <div class="page-contents max-width">
    <?php if (have_posts()) : ?>
      <?php while (have_posts()) : the_post(); ?>
        <?php the_content(); ?>
      <?php endwhile; ?>
    <?php endif; ?>
  </div>
  <!-- ADD PAGE CONTENT -->


</main>

<?php get_footer(); ?>