<?php /*
THE HEADER TEMPLATE FOR OUR THEME
*/ ?>

<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <title><?php wp_title( '|', true, 'right' ); ?></title>
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
  <?php endif; ?>
  <?php wp_head(); ?>
  
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-34405662-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-34405662-1');
</script>


<?php if(is_page('about')) { ?>
    <script src="https://d3js.org/d3.v3.min.js"></script>
    <script src="https://d3js.org/topojson.v1.min.js"></script>
    <script type="text/javascript" src='<?php bloginfo("stylesheet_directory"); ?>/js/planetaryjs.min.js'></script>
<?php } ?>
<!-- PLANETARY.JS -->

<!-- Hotjar Tracking Code for http://agmautomotive.com/ -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:587083,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>


<!-- FAVICON -->
  <link rel="icon" type="image/png" href="<?php bloginfo('stylesheet_directory'); ?>/img/favicon.png" />
<!-- MOBILE SITE MEDIA QUERY -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
<!-- FANCYBOX CSS -->
<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/jquery.fancybox.min.css" />
<!-- ANIMATIONS CSS -->
  <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/animate.min.css" />
<!-- SLICK SLIDER CSS -->
  <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/slick.min.css" />
<!-- MAIN CSS -->
  <link href="<?php bloginfo('stylesheet_url');?>" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/base-theme.min.css" />
  <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/custom.min.css" />
  <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/lyle.css" />
<!-- STYLEGUIDE CSS -->
  <?php get_template_part( 'template-parts/content', 'styleguide' ); ?>


</head>

<body <?php body_class(); ?>>

<!-- SITE NAVIGATION -->
  <?php get_template_part( 'template-parts/content', 'primary-nav' ); ?>