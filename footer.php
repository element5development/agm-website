<?php /*
THE FOOTER TEMPLATE FOR OUR THEME
*/ ?>

<!-- SITE NAVIGATION -->
  <?php get_template_part( 'template-parts/content', 'page-footer' ); ?>

<!-- FANCYBOX -->
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.fancybox.min.js"></script>
<!-- SMOOTH SCROLLING -->
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/smoothscroll.min.js"></script>
<!-- STICKY -->
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.sticky-kit.min.js"></script>
<!-- ENTRANCE ANIMATIONS -->
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.viewportchecker.min.js"></script>
<!-- SLICK SLIDERS -->
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/slick.min.js"></script>
<!-- DECLARATIONS -->
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/declarations.min.js"></script>
  
</body>

<?php wp_footer(); ?>

</html>