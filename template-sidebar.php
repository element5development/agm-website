<?php /*
Template Name: Content With Sidebar
*/ ?>

<?php get_header(); ?>

<main class="full-width">
  <div class="max-width clearfix">

    <div class="sidebar-page-contents <?php if( get_field('sidebar_location') == 'left' ) {?>right<?php }?> ">
      <!-- ADD PAGE CONTENT -->
      <div class="page-contents">
        <?php if (have_posts()) : ?>
          <?php while (have_posts()) : the_post(); ?>
            <?php the_content(); ?>
          <?php endwhile; ?>
        <?php endif; ?>
      </div>
      <!-- ADD PAGE CONTENT -->
    </div>

    <div class="sidebar <?php if( get_field('sidebar_location') == 'right' ) {?>right<?php }?>">
      <?php dynamic_sidebar( 'primary-left' ); ?>
    </div>

  </div>
</main>

<?php get_footer(); ?>