<?php /*
THE TEMPLATE FOR DISPLAYING SINGLE BLOG POST
*/ ?>

<?php get_header(); ?>

<main class="full-width">
  <?php get_template_part( 'template-parts/content', 'page-top' ); ?>
  <div class="blog-post max-width">
       <?php $thumbnail = get_the_post_thumbnail_url(); ?>
      <div class="cert-image">
        <img src="<?php echo $thumbnail; ?>">
      </div>
      <div class="cert-content">
        <p class='post-date'><?php echo get_the_date();?></p>
        <h3 class='post-title'><?php the_title(); ?></h3>
        <p class="post-content"><?php the_content(); ?></p>
      </div>
   </div>
</main>
<div style="clear: both;"></div>

<div class="certificates-container max-width">
	<h2 class="text-center">More posts you might like</h2>
	<?php 
		$args = array(
			'posts_per_page' => 2, 
			'category__in' => wp_get_post_categories($post->ID),
			'post__not_in' => array($post->ID)
		);
		$related = new WP_Query($args);
		if( $related->have_posts() ) { 
		  while( $related->have_posts() ) { $related->the_post(); ?>
		
  		<div class="certificate one-half">
     			<?php $thumbnail = get_the_post_thumbnail_url(); ?>
      		<div class="cert-image">
        		<img src="<?php echo $thumbnail; ?>">
      		</div>
      		<div class="cert-content">
        		<p class='post-date'><?php echo get_the_date();?></p>
        		<h3 class='post-title'><?php the_title(); ?></h3>
        		<p class='post-excerpt'><?php echo get_the_excerpt(); ?></p>
        		<a class='green-button' style="padding:15px 35px 15px 35px;" href="<?php echo get_permalink(); ?>">Read the Rest</a>
      		</div>
  		</div>

		  <?php }
		}
		wp_reset_postdata(); ?>

</div>
<div style="clear:both;"></div>
<?php get_footer(); ?>