<?php

//SETUP WORDPRESS & BACKEND
  // ENABLE FEATURED IMAGES
    add_theme_support( 'post-thumbnails' );
  // ENABLE JQUERY
    if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", 11);
    function my_jquery_enqueue() {
       wp_deregister_script('jquery');
       wp_register_script('jquery', "https" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js", false, null);
       wp_enqueue_script('jquery');
    }
  // ENABLE ACF OPTIONS PAGE
    if( function_exists('acf_add_options_page') ) {
      acf_add_options_page();
      acf_add_options_sub_page('Default Images');
      acf_add_options_sub_page('Theme Options');
      acf_add_options_sub_page('Styleguide');
    }
  // ALLOW SHORTCODES IN WIDGETS
    add_filter('widget_text', 'do_shortcode');
  // ALLOW SVG & ZIP FILES IN WORDPRESS
    function cc_mime_types($mimes) {
      $mimes['svg'] = 'image/svg+xml';
      $mimes['zip'] = 'application/zip';
      return $mimes;
    }
    add_filter('upload_mimes', 'cc_mime_types');
  // ALLOW EDITOR ACCESS TO GRAVITY FORMS, MENUS & WIDGETS
    function add_grav_forms(){
      $role = get_role('editor');
      $role->add_cap('gform_full_access');
      $role->add_cap('edit_theme_options');
      $role->add_cap('edit_users');
      $role->add_cap('list_users');
      $role->add_cap('promote_users');
      $role->add_cap('create_users');
      $role->add_cap('add_users');
      $role->add_cap('delete_users');
    }
    add_action('admin_init','add_grav_forms');
  // ADD SMOOTHSCROLL CLASS TO MENU LINKS WITH REL
      function add_menuclass($ulclass) {
        return preg_replace('/<a rel="smoothScroll"/', '<a rel="smoothScroll" class="smoothScroll"', $ulclass);
      }
      add_filter('wp_nav_menu','add_menuclass');
  //ADD ACF TO BE USED IN THE RELEVANSSI CUSTOM EXCERPTS
    add_filter('relevanssi_excerpt_content', 'custom_fields_to_excerpts', 10, 3);
    function custom_fields_to_excerpts($content, $post, $query) {
        $custom_fields = get_post_custom_keys($post->ID);
        $remove_underscore_fields = true;
        if (is_array($custom_fields)) {
          $custom_fields = array_unique($custom_fields);  // no reason to index duplicates
          foreach ($custom_fields as $field) {
            if ($remove_underscore_fields) {
              if (substr($field, 0, 1) == '_') continue;
            }
            $values = get_post_meta($post->ID, $field, false);
            if ("" == $values) continue;
            foreach ($values as $value) {
              if ( !is_array ( $value ) ) {
                $content .= " | " . $value;
              }
            }
          }
        }
        return $content;
    }
  // STYLEGUIDE HEX TO RGB
    function hex2rgb( $colour ) {
      if ( $colour[0] == '#' ) { $colour = substr( $colour, 1 ); }
      if ( strlen( $colour ) == 6 ) {
        list( $r, $g, $b ) = array( $colour[0] . $colour[1], $colour[2] . $colour[3], $colour[4] . $colour[5] );
      } elseif ( strlen( $colour ) == 3 ) {
        list( $r, $g, $b ) = array( $colour[0] . $colour[0], $colour[1] . $colour[1], $colour[2] . $colour[2] );
      } else {
        return false;
      }
      $r = hexdec( $r ); $g = hexdec( $g ); $b = hexdec( $b );
      return 'rgb('.$r.', '.$g.', '.$b.')';
    }
  // STYLEGUIDE RGB TO CMYK
    function hex2rgb2($hex) {
      $color = str_replace('#','',$hex);
      $rgb = array(
        'r' => hexdec(substr($color,0,2)),
        'g' => hexdec(substr($color,2,2)),
        'b' => hexdec(substr($color,4,2)),
      );
      return $rgb;
    }
    function rgb2cmyk($var1,$g=0,$b=0) {
      if (is_array($var1)) { $r = $var1['r']; $g = $var1['g']; $b = $var1['b'];
      } else { $r = $var1; }
      $r = $r / 255; $g = $g / 255; $b = $b / 255;
      $bl = 1 - max(array($r,$g,$b));
      $c = ( 1 - $r - $bl ) / ( 1 - $bl ); $m = ( 1 - $g - $bl ) / ( 1 - $bl ); $y = ( 1 - $b - $bl ) / ( 1 - $bl );
      $c = round($c * 100); $m = round($m * 100); $y = round($y * 100); $bl = round($bl * 100);
      return 'cmyk('.$c.', '.$m.', '.$y.', '.$bl.')';
    }
//SETUP WORDPRESS & BACKEND


//CUSTOMIZE CONTENT FOR CLIENT
  // ADD MENUS FOR NAVIGATION
    function register_menu () {
      register_nav_menu('primary-menu', __('Primary Menu'));
      register_nav_menu('footer-menu', __('Footer Menu'));
    }
    add_action('init', 'register_menu');
  // ADD WIDGET AREAS
    function themename_widgets_init() {
      //LEFT SIDEBAR
      register_sidebar( array(
          'name'          => __( 'Primary Sidebar', 'theme_name' ),
          'id'            => 'primary-left',
          'before_widget' => '<aside id="%1$s" class="widget %2$s">',
          'after_widget'  => '</aside>',
          'before_title'  => '<h3 class="widget-title">',
          'after_title'   => '</h3>',
      ) );
    }
    add_action( 'widgets_init', 'themename_widgets_init' );
  // PRIMARY BUTTON SHORTCODE
    function primary_button($atts, $content = null) {
      extract( shortcode_atts( array(
        'target' => '',
        'url' => '#',
        'arrow' => ''
      ), $atts ) );
      return '<a target="'.$target.'" href="'.$url.'" class="primary-button '.$arrow.'">' . do_shortcode($content) . '</a>';
    }
    add_shortcode('primary-btn', 'primary_button');
  // SECONDARY BUTTON SHORTCODE
    function secondary_button($atts, $content = null) {
      extract( shortcode_atts( array(
        'target' => '',
        'url' => '#',
        'arrow' => ''
      ), $atts ) );
      return '<a target="'.$target.'" href="'.$url.'" class="secondary-button '.$arrow.'">' . do_shortcode($content) . '</a>';
    }
    add_shortcode('secondary-btn', 'secondary_button');
  // LEAGAL TEXT SHORTCODE
    function legal_text($atts, $content = null) {
      extract( shortcode_atts( array( ), $atts ) );
      return '<p class="legal-text">' . do_shortcode($content) . '</p>';
    }
    add_shortcode('legal-text', 'legal_text');
  //ADD CUSTOM POST TYPE
    function create_posttype() {
      // Case Studies
      $args = array(
          'labels' => array(
            'name'                => __( 'Products' ),
            'singular_name'       => __( 'Product' )
					),
          'supports'            => array( 'title', 'editor', 'thumbnail' ),
          'taxonomies'          => array( 'category' ),
          'hierarchical'        => false,
          'public'              => true,
          'has_archive'         => false,
          'exclude_from_search' => false,
      );
      register_post_type( 'products', $args );
      //STAFF
      register_post_type( 'staff',
        array(
          'labels' => array(
            'name' => __( 'Staff' ),
            'singular_name' => __( 'Staff' )
          ),
          'supports' => array( 'title', 'editor', 'thumbnail' ),
          'public' => true,
          'has_archive' => false,
          'rewrite' => array('slug' => 'staff'),
        )
      );
    }
    add_action( 'init', 'create_posttype' );
//CUSTOMIZE CONTENT FOR CLIENT

?>