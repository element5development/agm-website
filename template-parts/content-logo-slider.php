<?php /*
SLICK SLIDER OF LOGOS FROM THEME OPTIONS
*/ ?>
  <div class="logo-slider turquoise-overlay">
    <div class="max-width">

  <?php if( have_rows('logos', 'options') ) {
      while ( have_rows('logos', 'options') ) : the_row(); ?>
        <div class="logo-slide">
          <?php $image = get_sub_field('logo'); ?>
          <a href="<?php the_sub_field('logo_link') ?>" target="_blank"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"/></a>
        </div>
      <?php endwhile;
  } else {
      //NOTHING
  } ?>
  </div>
</div>