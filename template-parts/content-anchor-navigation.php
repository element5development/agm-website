<?php /*
DISPLAY ANCHOR NAVIGATION
*/ ?>


<div class="navigation-container full-width">
  <?php if( have_rows('navigation') ){ ?>
  <?php $anchor_num = 0; ?>
    <ul class='anchor-navigation max-width'>
    <?php while ( have_rows('navigation') ) : the_row(); ?>
    <?php $anchor_num = $anchor_num+1;
    ?>
      <li class="single-anchor-nav">
        <a href="#<?php echo $anchor_num; ?>" class="smoothScroll"><?php the_sub_field('nav'); ?></a>
      </li>

    <?php endwhile; ?>
    </ul>
 <?php  } else {
      // no rows found
  } ?>
</div>
