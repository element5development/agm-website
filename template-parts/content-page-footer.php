<?php /*
FOOTER CONTENT
*/ ?>

<?php if (  is_page_template( 'template-styleguide.php' ) ) { //STYLEGUIDE SIMPLIFIED FOOTER ?>
  <footer id="footer" class="full-width">
    <div class="max-width clearfix">
      <p id="copyright">©Copyright <?php echo date('Y'); ?> AGM Automotive - A Flex Company. All Rights Reserved.</p> 
      <a href="https://element5digital.com/" target="_blank" title="Element5 provides content and search marketing, web development and brand strategy." id="created-e5"><p>
        Designed & Developed by <img src="<?php bloginfo('stylesheet_directory'); ?>/img/e5-footer-white.svg" alt="Element5 Crafting a better web to help brands thrive in a digital world." height="45">
      </p></a>
    </div>
  </footer>
<?php } else { ?>
  <footer id="footer" class="full-width">
    <div class='max-width footer-logo'>
        <a href="/"><img src="<?php bloginfo('stylesheet_directory'); ?>/img/main-nav-logo.png"></a>
        <a href="https://www.flex.com/" target="_blank"><img src="<?php bloginfo('stylesheet_directory'); ?>/img/flex-logo-for-footer.png" alt="flex logo"></a>
    </div>

    <div class="max-width clearfix">
      <nav>
        <?php wp_nav_menu( array( 'theme_location' => 'footer-menu' ) ); ?>
      </nav>
      <!-- <ul class="footer-social">
        <li><a href="https://www.facebook.com/agminc" target="_blank"><img id="facebook-footer" src="<?php bloginfo('stylesheet_directory'); ?>/img/facebook-black.png" alt="facebook"/></a></li>
        <li><a href="https://twitter.com/AGM_HQ" target="_blank"><img id="twitter-footer" src="<?php bloginfo('stylesheet_directory'); ?>/img/twitter-black.png" alt="twitter"/></a></li>
        <li><a href="https://www.linkedin.com/company-beta/151067?pathWildcard=151067" target="_blank"><img id="linkedin-footer" src="<?php bloginfo('stylesheet_directory'); ?>/img/linkedin-black.png" alt="linkedin"/></a></li>
      </ul> -->
    </div>
    <div class="full-width page-bottom">
      <div class="max-width clearfix">
        <p id="copyright">©<?php echo date('Y'); ?> AGM Automotive - A Flex Company. All Rights Reserved. | <a href="/terms-and-conditions">Terms & Conditions</a> | <a href="/privacy-policy">Privacy Policy</a></p> 
        <a href="https://element5digital.com/" target="_blank" title="Element5 provides content and search marketing, web development and brand strategy." id="created-e5"><p>
        Designed & Developed by <img src="<?php bloginfo('stylesheet_directory'); ?>/img/e5-footer-white.svg" alt="Element5 Crafting a better web to help brands thrive in a digital world." height="45">
       </p></a>
      </div>
    </div>
  </footer>
<?php } ?>