<?php /*
DISPLAY LEADERSHIP BIO AND TEAM EMMBER HEADSHOTS
*/ ?>

<div class="leadership-bio-container max-width">
	<div id="leadership-back">
		<img src="<?php bloginfo('stylesheet_directory'); ?>/img/previous-arrow.svg" alt="back to leadership"><p>BACK TO LEADERSHIP TEAM</p>
	</div>
	<?php if( have_rows('leadership') ){
    while ( have_rows('leadership') ) : the_row(); ?>
		<div class="leadership-bio clearfix">
			<div class="one-third">
				<?php $image = get_sub_field('leader_headshot'); ?><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"/>
			</div>
			<div class="two-third">
				<h2><?php the_sub_field('leader_name'); ?></h2>
          		<h4><?php the_sub_field('leader_title'); ?></h4>
				<p><?php the_sub_field('leader_bio'); ?></p>
			</div>
		</div>
	<?php endwhile; ?>
	<? } else {
      // no rows found
  } ?>
</div>