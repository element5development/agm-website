<?php /*
DISPLAYED WHEN NO CONTENT IS FOUND FOR QUERY
*/ ?>

<article>
	<h2>Nothing Found </h2>
	        <div class="search-bar">
        	      <form role="search" method="get" class="search-form" action="https://agmautomotive.com/">
        <label>
          <span class="screen-reader-text">Search for:</span>
          <input type="search" class="search-field" placeholder="Search &hellip;" value="" name="s" />
        </label>
        <button type="submit" class="search-submit"><span class="screen-reader-text">Search</span></button>
      </form>
      	</div>
</article>