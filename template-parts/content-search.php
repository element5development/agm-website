<?php //SEARCH RESULTS
?>
<a href="<?php echo get_permalink();?>" class="one-half">
  <div class="certificate">
     <?php $thumbnail = get_the_post_thumbnail_url(); ?>
      <div class="cert-image">
        <img src="<?php echo $thumbnail; ?>">
      </div>
      <div class="cert-content">
        <p class='post-date'><?php echo get_the_date();?></p>
        <h3 class='post-title'><?php the_title(); ?></h3>
        <p class='post-excerpt'><?php echo get_the_excerpt(); ?></p>
      </div>
  </div>
 </a>

