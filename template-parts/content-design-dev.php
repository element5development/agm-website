<?php /*
REDESIGN OF DESIGN, DEV, PROTYPTING SECTION ON ENGINEERING PAGE
*/ ?>
<div class="page-cards-container max-width">
<a id="3" class="anchor"></a>
    <h2 style='text-align:center;margin: 0 auto;
    padding-bottom: 75px;padding-top: 25px;'>Design, Development, Prototyping, & Validation Toolbox
    </h2>
<div class="design-container">
<?php if( have_rows('cards1') ) { 
  $num_rows = 0;
  $category='';
  $cat_class='';
  ?>
  <?php while(have_rows('cards1')) { the_row();
    $num_rows = $num_rows+1;
  }?>
    <div class="design-dev-section1
    <?php if($num_rows==1) { ?>
      one-fourth
   <?php } elseif($num_rows==2){ ?>
      one-half
   <?php } elseif ($num_rows==3) { ?>
    three-fourth
   <?php } else { ?>
    full-width
   <?php } ?>
    ">
      <h3 id="design-dev">Design &amp; Development </h3>
      <div class="content-section1">
    <?php while ( have_rows('cards1') ) { the_row(); 
      $category=get_sub_field('card_title');
      ?>
        <div class="page-card">
          <?php if(get_sub_field('card_image')){
              $image = get_sub_field('card_image'); 
           } ?>
          <div class="page-card-img" style="background-image: url('<?php echo $image['url']; ?>');">
            <?php if(get_sub_field('card_title')){?><h2><?php the_sub_field('card_title'); ?></h2><?php } ?>
          </div>

          <?php if(get_sub_field('card_title')){?>
          <div class="page-card-content <?php echo $cat_class ?>">
            <?php the_sub_field('card_description'); ?>

          </div>

          <?php } ?>
          </div>

    <?php } ?>
            </div>

  </div>
<?php } ?>
<?php if( have_rows('cards2') ) { 
  $category='';
  $cat_class='';
  $num_rows = 0;
  ?>
    <?php while(have_rows('cards2')) { the_row();
    $num_rows = $num_rows+1;
  }?>
   <div class="design-dev-section2
    <?php if($num_rows==1) { ?>
      one-fourth
   <?php } elseif($num_rows==2){ ?>
      one-half
   <?php } elseif ($num_rows==3) { ?>
    three-fourth
   <?php } else { ?>
    full-width
   <?php } ?>
   ">
       <h3 id="prototyping">Prototyping</h3>
       <div class="content-section2">
    <?php while ( have_rows('cards2') ) { the_row(); 
      $category=get_sub_field('card_title');
      ?>

      <div class="page-card">
        <?php if(get_sub_field('card_image')){
            $image = get_sub_field('card_image'); 
         } ?>
        <div class="page-card-img" style="background-image: url('<?php echo $image['url']; ?>');">
          <?php if(get_sub_field('card_title')){?><h2><?php the_sub_field('card_title'); ?></h2><?php } ?>
        </div>

        <?php if(get_sub_field('card_title')){?>
        <div class="page-card-content <?php echo $cat_class ?>">
          <?php the_sub_field('card_description'); ?>

        </div>

        <?php } ?>
      </div>

    <?php } ?>
    </div>
  </div>
<?php } ?>
</div>
<?php if( have_rows('cards3') ) { 
  $category='';
  $cat_class='';
  $num_rows=0;
  ?>
    <?php while(have_rows('cards3')) { the_row();
    $num_rows = $num_rows+1;
  }?>
  <div class="design-dev-section3
    <?php if($num_rows==1) { ?>
      one-fourth
   <?php } elseif($num_rows==2){ ?>
      one-half
   <?php } elseif ($num_rows==3) { ?>
    three-fourth
   <?php } else { ?>
    full-width
   <?php } ?>
  ">
      <h3 id="validation">Validation </h3>
    <div class="content-section3">
    <?php while ( have_rows('cards3') ) { the_row(); 
      $category=get_sub_field('card_title');
      ?>

      <div class="page-card">
        <?php if(get_sub_field('card_image')){
            $image = get_sub_field('card_image'); 
         } ?>
        <div class="page-card-img" style="background-image: url('<?php echo $image['url']; ?>');">
          <?php if(get_sub_field('card_title')){?><h2><?php the_sub_field('card_title'); ?></h2><?php } ?>
        </div>

        <?php if(get_sub_field('card_title')){?>
        <div class="page-card-content <?php echo $cat_class ?>">
          <?php the_sub_field('card_description'); ?>

        </div>

        <?php } ?>
      </div>

    <?php } ?>
    </div>
  </div>
<?php } ?>

    
