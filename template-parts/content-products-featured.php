<?php /*
DISPLAY FEATURED PRODUCTS
*/ ?>
<div class='max-width featured-products clearfix'>
  <h2>Featured Products</h2>
  <ul class="product-categories">
    <li class="door-lighting active">Door Lighting</li>
    <li class="overhead-systems">Overhead Systems</li>
    <li class="carpeting">Textiles</li>
    <li class="instrument-panels">Instrument Panels and Floor Consoles</li>
  </ul>
<!--   <div class="product-categories-mobile">
    <li class="door-lighting active">Door Lighting</li>
    <li class="overhead-systems">Overhead Systems</li>
    <li class="carpeting">Textiles</li>
    <li class="instrument-panels">Instrument Panels and Floor Consoles</li>
  </div> -->
     
  <?php  //PULL FEATURED PRODUCTS
    $args = array(
      'posts_per_page' => '-1',
      'post_type'   => 'products',
      'meta_query' => array(
        array(
          'key' => 'featured',
          'value' => 'Yes',
        )
      )
    );
    $the_query = new WP_Query( $args );
  ?>
  <?php if( $the_query->have_posts() ) { ?>
    <?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
      <?php 
      // SHOW YOAST PRIMARY CATEGORY, OR FIRST CATEGORY
        $category = get_the_category();
        $useCatLink = false;
        $cat_class='';
        $product_id = get_the_id();
      // If post has a category assigned.
        if ($category){
          $category_display = '';
          $category_link = '';
          if ( class_exists('WPSEO_Primary_Term') )
            {
          // Show the post's 'Primary' category, if this Yoast feature is available, & one is set
            $wpseo_primary_term = new WPSEO_Primary_Term( 'category', get_the_id() );
            $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
            $term = get_term( $wpseo_primary_term );
            if (is_wp_error($term)) { 
           // Default to first category (not Yoast) if an error is returned
              $category_display = $category[0]->name;
              $category_link = get_category_link( $category[0]->term_id );
            } else { 
          // Yoast Primary category
              $category_display = $term->name;
              $category_link = get_category_link( $term->term_id );
            }
          } 
          else {
        // Default, display the first category in WP's list of assigned categories
            $category_display = $category[0]->name;
            $category_link = get_category_link( $category[0]->term_id );
          }
          if($category_display==='Door Lighting') {
            $cat_class='door selected';
            $pass_cat = 'door';
          } elseif ($category_display==='Overhead Systems') {
            $cat_class='overhead';
            $pass_cat = 'overhead';
          } elseif ($category_display==='Textiles') {
            $cat_class='carpet';
            $pass_cat = 'carpet';
          } else {
            $cat_class='instrument';
            $pass_cat = 'instrument';
          }
        } ?> 

      <div class="single-product-preview <?php echo $cat_class; ?>">
      <?php 
        if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it. 
                $alt_text = get_post_meta($post->ID, '_wp_attachment_image_alt', true); ?>
          <img src='<?php the_post_thumbnail_url(); ?>' alt="<?php echo $alt_text; ?>"/>
        <?php } ?>  

     <?php 
      // Display category
        if ( !empty($category_display) ){ ?>
          <p><?php echo $category_display; ?></p>
        <?php } else {
            // nothing
        }?>
        <h1><?php the_title(); ?></h1>
        <a href="products/?product=<?php echo $product_id; ?>&category=<?php echo $pass_cat; ?>#content" class="green-button" id='<?php echo $product_id; ?>'>LEARN MORE</a>
      </div>

    <?php endwhile; ?>
  <?php } ?>
  <?php wp_reset_query();  // Restore global post data stomped by the_post(). ?>
</div>