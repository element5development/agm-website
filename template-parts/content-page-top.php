<?php /*
DISPLAY PAGE TITLE, FEATURED IMAGE, SLIDER, ETC.
*/ ?>

<?php  //DETERMINE IMAGE
if (has_post_thumbnail( $post->ID ) ) { //USE FEATURED IMAGE
   $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
} else { //USE DEFAULT IMAGES
    if ( is_archive() || is_home() ) { 
      $image = get_field('featured_news_image', 'options');
    } elseif ( is_single() ) { 
      $image = get_field('featured_news_image', 'options');
    } else {
      $image = get_field('default_page_image', 'options');
    }
  }
?>

<?php if ( is_front_page() ) { //HOME PAGE ?>
  <div class="page-heading vertical-align-parent" style="background-image: url(/wp-content/uploads/2016/10/winding-road.jpg);">
    <!-- <div class="video-wrap"> 
      <video muted="" autoplay="" loop="" poster="/wp-content/uploads/2017/04/homepg-car-dark-blue7.jpg" class="bgvid"> 
          <source src="https://e5agm.wpengine.com/wp-content/themes/AGM/video/agm-vid-final.mp4" type="video/mp4">
          <source src="https://e5agm.wpengine.com/wp-content/themes/AGM/video/agm-vid-final.webm" type="video/webm">
      </video>
    </div> -->

    <div class="vertical-align-child">
      <?php if ( get_field('title') ) { ?> <h1> <?php echo get_field('title'); ?> </h1> <?php } ?>
      <div class="line"></div>
      <?php if ( get_field('sub-title') ) { ?> <p> <?php echo get_field('sub-title'); ?> </p> <?php } ?>
      <?php $link = get_field('button_link'); ?>

			<?php if( !empty($link) ): ?>
				<a class="glow" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"> <?php echo get_field('button_text'); ?> </a>
			<?php endif; ?>
    </div>
  </div> 
<?php } elseif ( is_archive() || is_home() ) { //BLOG PAGE ?>
  <div class="page-heading vertical-align-parent" style="background-image: url('/wp-content/themes/AGM/img/news-background.png')">
    <div class="vertical-align-child">
      <h1 style="text-align: center;">AGM News</h1>
      <div class="line"></div>
    </div>
  </div>
<?php } elseif (is_search()) { ?>
  <div class="page-heading vertical-align-parent" style="background-image: url('/wp-content/themes/AGM/img/news-background.png')">
    <div class="vertical-align-child">
            <h1 style="text-align: center;">You Searched for: <?php the_search_query(); ?></h1>
            <div class='line'></div>
      </div>  
    </div>
<?php } elseif ( is_single() ) { //POST PAGE ?>
  <div class="page-heading vertical-align-parent" style="background-image: url('/wp-content/themes/AGM/img/news-background.png')">
    <div class="vertical-align-child">
      <h1 style="text-align: center;">AGM News</h1>
      <div class="line"></div>
      <?php if ( get_field('sub-title') ) { ?> <p> <?php echo get_field('sub-title'); ?> </p> <?php } ?>
      <?php if ( get_field('button_text') && get_field('button_link')) { ?> 
        <a href="<?php echo get_field('button_link'); ?>" class="glow"> <?php echo get_field('button_text'); ?> </a> <?php } ?>
    </div>
  </div>
<?php } elseif ( is_page(2315) || is_page(1615) ) { //ABOUT & CAREERS VIDEO HEADER ?>
  <div class="page-heading vertical-align-parent" style="background-image: url('<?php echo $image[0]; ?>');z-index: 0;">
    <div class="vertical-align-child">
      <?php if ( get_field('title') ) { ?> <h1> <?php echo get_field('title'); ?> </h1> <?php } else { ?>
        <h1><?php the_title(); ?></h1>
      <?php } ?>
      <div class="line"></div>
      <?php if ( get_field('sub-title') ) { ?> <p> <?php echo get_field('sub-title'); ?> </p> <?php } ?>
      <?php if ( get_field('button_text') && get_field('button_link')) { ?> 
        <a href="<?php echo get_field('button_link'); ?>" class="glow"> <?php echo get_field('button_text'); ?> </a> 
      <?php } ?>
    </div>
    <div class="video-wrap"> 
      <video muted="" autoplay="" loop="" poster="<?php echo $image[0]; ?>" class="bgvid"> 
        <source src="/https://agmautomotive.com/wp-content/themes/AGM/video/agm-page-vid.mp4" type="video/mp4"> 
        <source src="https://agmautomotive.com/wp-content/themes/AGM/video/agm-page-vid.webm" type="video/webm"> 
      </video> 
    </div>
  </div>
<?php } else { //PAGES ?>
  <div class="page-heading vertical-align-parent" style="background-image: url('<?php echo $image[0]; ?>')">
    <div class="vertical-align-child">
      <?php if ( get_field('title') ) { ?> <h1> <?php echo get_field('title'); ?> </h1> <?php } else { ?>
        <h1><?php the_title(); ?></h1>
      <?php } ?>
      <div class="line"></div>
      <?php if ( get_field('sub-title') ) { ?> <p> <?php echo get_field('sub-title'); ?> </p> <?php } ?>

			<?php $link = get_field('button_link'); ?>

			<?php if( !empty($link) ): ?>
				<a class="glow" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"> <?php echo get_field('button_text'); ?> </a>
			<?php endif; ?>

    </div>
  </div>
<?php } ?>