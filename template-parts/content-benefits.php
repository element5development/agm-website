<?php /*
DISPLAY BENEFITS
*/ ?>
<div class="benefits-container full-width">
<a id="2" class="anchor smoothScroll"></a>

  <?php if ( get_field('icon') ) { ?> <img src="<?php echo get_field('icon'); ?>"> <?php } ?>
  <?php if ( get_field('benefits_title') ) { ?> <h2> <?php echo get_field('benefits_title'); ?> </h2> <?php } ?>
  <?php if( have_rows('benefits') ){ ?>
    <?php if(is_page('capabilities')){ ?>
      <ul class='benefits max-width'>
        <?php while ( have_rows('benefits') ) : the_row();
         $category=get_sub_field('benefit');
        if($category==='Injection Molding'){
          $category='injmold active';
        } elseif($category==='Plastic Decorating') {
          $category='plasdec';
        } elseif($category==='Electronics Assembly'){
          $category='electcomp';
        } elseif($category==='Value-Added Assembly'){
          $category='valueaa';
        } elseif ($category==='Textile Processing') {
          $category='textproc';
        }?>
      <li class="single-benefit <?php echo $category; ?>">
        <a href="#3" class="smoothScroll capabilities-link">
        <?php if(get_sub_field('benefit_icon')) { ?>
          <img src="<?php the_sub_field('benefit_icon'); ?>"/>
        <?php } ?>
       <?php the_sub_field('benefit'); ?>
       </a>
      </li>

    <?php endwhile; ?>
    </ul>    
    <?php } else { ?>
      <ul class='benefits max-width'>
    <?php while ( have_rows('benefits') ) : the_row(); ?>
      <li class="single-benefit <?php echo $category; ?>">
        <?php if(get_sub_field('benefit_icon')) { ?>
          <img src="<?php the_sub_field('benefit_icon'); ?>"/>
        <?php } ?>
       <?php the_sub_field('benefit'); ?>
      </li>

    <?php endwhile; ?>
    </ul>
    <?php } ?>
 <?php  } else {
      // no rows found
  } ?>
</div>