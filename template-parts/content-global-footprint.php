<section class="global-footprint full-width">
<a id="2" class="anchor"></a>
	<div class="global-container max-width">
		<h2>Global Footprint</h2>
		<div class="line"></div>
		<p>AGM, Applied Global Manufacturing, is a worldwide organization. 
With locations in the United States, Mexico, Costa Rica, China, and Europe, AGM is home to over 1,400 employees. 
</p>
		<img src="<?php bloginfo('stylesheet_directory'); ?>/img/turn-globe.png"/ alt="controls to turn globe">
		<canvas id='globe' width='500' height='500'
  style='width: 500px; height: 500px; cursor: move;'></canvas>
  </div>
</section>
<?php /*
SLICK SLIDER OF AWARDS FROM QUALITY PAGE
*/ ?>
  <div class="locations-slider full-width locations-container">

<?php  
  $rows = get_field('location');
  $length = count($rows);
  $row_num = 0;
  if ($rows)
  {
    foreach($rows as $row)
    {
      
      $prev_num = $row_num - 1; //GET PREVIOUS ROW
      if ( $prev_num < 0 ) { $prev_num = $length - 1; } //IF ON FIRST ROW THEN PULL LAST ROW INFO
      $prev_row = $rows[$prev_num]; 
      $prev_row_location = $prev_row['location_csc' ];
      $next_num = $row_num + 1; //GET NEXT ROW
      if ( $next_num == $length ) { $next_num = 0; } //IF ON LAST ROW THEN PULL FIRST ROW INFO
      $next_row = $rows[$next_num]; 
      $next_row_location = $next_row['location_csc' ];
      ?>

      <div class="location-slide">
        <div class="max-width">
          <div class="one-half">
            <div class="location-img" style="background-image: url('<?php echo $row['location_image']; ?>');"></div>
          </div>
          <div class="one-half">
            <p><?php echo $row['location_name']; ?></p>
            <h2><?php echo $row['location_csc']; ?></h2>
            <p><?php echo $row['location_description']; ?></p>
          </div>
        </div>
        <div class="prev_row_location"><?php echo $prev_row_location; ?></div>
        <div class="next_row_location"><?php echo $next_row_location;; ?></div>
      </div>

      <?php
      $row_num++;
    }
  }
?>




</div>