<?php /*
FEATURED BLOG POST PREVIEW AS SHOWN ON THE NEWS/BLOG PAGE
*/ ?>
<?php 
  $args = array(
    'numberposts' => 1,
    'post_type'   => 'post',
    'meta_key'    => 'featured',
    'meta_value'  => 'Yes'
  );
  $the_query = new WP_Query( $args ); 
  if( $the_query->have_posts() ) {  //FEATURED POST ?>
  <div class="full-width featured-post">
    <div class="max-width featured-post clearfix">
    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
       <?php $thumbnail = get_the_post_thumbnail_url(); ?>
      <h2 style="text-align: center;">Featured Article</h2>
      <div class="one-half featured-image">
        <img src="<?php echo $thumbnail; ?>">
      </div>
      <div class="one-half featured-post-content">
        <p class='post-date'><?php echo get_the_date();?></p>
        <h3 class='post-title'><?php the_title(); ?></h3>
        <p class='post-excerpt'><?php echo get_the_excerpt(); ?></p>
        <a class='green-button' style="padding:15px 35px 15px 35px;" href="<?php echo get_permalink(); ?>">Read the Rest</a>
      </div>
    <?php endwhile; ?>
    </div>
  </div>
  <?php } ?>