<?php /*
DISPLAY PROFESSIONAL CERTIFICATIONS
*/ ?>

<?php if( have_rows('certifications') ) { ?>
  <div class="certificates-container max-width">
  <?php if(is_page('quality')){ ?>
        <h2>Professional Certifications</h2>
  <?php } ?>

    <?php while ( have_rows('certifications') ) { the_row(); ?>

        <div class="certificate one-half">
          <?php $image = get_sub_field('certificate_image'); ?>
          <div class="cert-image"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></div>
          <div class="cert-content">
            <p><?php the_sub_field('certificate_title'); ?></p>
            <h3><?php the_sub_field('certificate_description'); ?></h3>
            <a href="<?php the_sub_field('certificate_link'); ?>" class="green-button" target="_blank">View PDF</a>
          </div>
        </div>
    <?php } ?>
  </div>
<?php } else {
    // no rows found
} ?>