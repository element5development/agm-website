<?php /*ORIGINITAVITY SECTION ON ABOUT PAGE */ ?>
<section class="originativity-container full-width">
<a id="3" class="anchor"></a>

	<div class="max-width">
		<img src="<?php echo get_field('originativity_logo');?>" alt="originativity">
		<h3><?php echo get_field('originativity_headline'); ?></h3>
		<p><?php echo get_field('originativity_content'); ?></p>
		<h2><?php echo get_field('originativity_sub_heading'); ?></h2> 
		<ul>
			<li><?php echo get_field('prove_it_1'); ?></li> 
			<li><?php echo get_field('prove_it_2'); ?></li>
			<li><?php echo get_field('prove_it_3'); ?></li>
		</ul>
	</div>
</section>
