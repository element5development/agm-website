<?php /*
DISPLAY LEADERSHIP BIO AND TEAM EMMBER HEADSHOTS
*/ ?>


<div class="certificates-container max-width">
<a id="4" class="anchor"></a>
      <?php 
        $args = array(
            'post_type' => 'staff',
            'posts_per_page' => -1,
            'order' => 'ASC'
        );
        $the_query = new WP_Query( $args );
      ?>

      <?php if ( $the_query->have_posts() ) : ?>
        <h2 style="text-align: center;">Our Executive Committee</h2>
        <div class="staff-loop-container full-width">
        	<div class="bios">
						<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
							<?php 
								if ( has_post_thumbnail() ) {
									$thumb_id = get_post_thumbnail_id();
									$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'large', true);
									$thumb_url = $thumb_url_array[0];
								}
							?>
							<div class="leadership-preview certificate one-half">
								<div class="leadership-image" style="background-image: url('<?php echo $thumb_url ?>');"></div>
								<div class="cert-content">
									<h3><?php the_title(''); ?></h3>
									<p><?php the_field('positiontitle'); ?></p>
									<a class="open-bio green-button">Learn More</a>
								</div>
							</div>
						<?php endwhile; ?>
					</div>
				</div>
      <?php else : ?>
		<?php endif; ?>
  <div class="join-staff full-width">
    <h3>Think you have what it takes to join our team?</h3>
    <p>Check out our LinkedIn page to apply.</p>
    <a href="https://www.linkedin.com/company-beta/151067?pathWildcard=151067" target="_blank" class="glow">Apply Now</a>
  </div>
  <div class="bio-overlay-slider">
    <div class="vertical-align-parent">
      <div class="vertical-align-child">
        <div class="bio-slider">
          <?php 
            $args = array(
                'post_type' => 'staff',
                'posts_per_page' => -1,
                'order' => 'ASC'
            );
            $the_query = new WP_Query( $args );
          ?>
          <?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>


              <?php 
                $next_post = get_next_post(); 
                $next_post_title = $next_post->post_title;
                $prev_post = get_previous_post();
                $prev_post_title = $prev_post->post_title;
                if ( has_post_thumbnail() ) {
                  $thumb_id = get_post_thumbnail_id();
                  $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'large', true);
                  $thumb_url = $thumb_url_array[0];
                }
              ?>

              <div class="single-bio">
                <div class="overlay-close"><span></span>Back to Leadership</div>
                <div class="leadership-image" style="background-image: url('<?php echo $thumb_url ?>');"></div>
                <div class="bio-info">
                  <h3><?php the_title(''); ?></h3>
                  <p><?php the_field('positiontitle'); ?></p>
                  <?php the_content(); ?>
                </div>
                <div style="clear: both;"></div>
                <div class="previous-next clearfix">
                  <div class="one-half"><?php echo $prev_post_title; ?></div>
                  <div class="one-half"><?php echo $next_post_title; ?></div>
                </div>
              </div>


          <?php endwhile; else : ?>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</div>