  <div class="page-heading join-team vertical-align-parent" style="background-image:url('http://e5agm.wpengine.com/wp-content/themes/AGM/img/join-team-background.png')">
    <div class="vertical-align-child">
      <h1>Want to join our team?</h1>
      <div class="line"></div>
      <!-- <p> Check out our LinkedIn page to apply.</p> -->
      <a href="https://flextronics.wd1.myworkdayjobs.com/en-US/AGM_Careers" class="glow" target="_blank">View Job Openings</a>
      <p>AGM Automotive is committed to a policy of equal employment opportunity and will not discriminate on any legally recognized basis, including but not limited to: race, age, skin color, religion, sex, marital satus, national origin, citizenship, ancestry, physical or mental disability, veteran status, or any other basis recognized by federal, state, or local law.</p>
    </div>
  </div> 