<?php /*
DISPLAY LOCATION CONTACT INFORMATION BASED ON REGION | DISPLAY MAP WITH LOCATIONS PINNED
*/ ?>

<?php if ( have_rows('na_locations') ) { ?>
<div class="north-america selected">
  <div class="location-map ">
    <?php the_field('na_iframe'); ?>
  </div>
  <div class="locations-container full-width">
    <div class="max-width">
    <h2>North America</h2>
    <?php while ( have_rows('na_locations') ) { the_row(); ?>

      <div class="one-fifth">
        <?php if ( get_sub_field('location_name') ) { ?><h3><?php the_sub_field('location_name'); ?></h3><?php } ?>
        <?php if ( get_sub_field('location_address') ) { ?><address><?php the_sub_field('location_address'); ?></address> <?php } ?>
        <?php if ( get_sub_field('location_phone') ) { ?><a href="tel:<?php the_sub_field('location_phone'); ?>">TEL <?php the_sub_field('location_phone'); ?></a> <?php } ?>
        <?php if ( get_sub_field('location_fax') ) { ?><a>FAX <?php the_sub_field('location_fax'); ?></a><?php } ?>
      </div>

    <?php } ?>
    </div>
  </div>
</div>
<?php } else {
  // no rows found
} ?>

<?php if ( have_rows('europe_locations') ) { ?>
<div class="europe">
  <div class="location-map">
    <?php the_field('eu_iframe'); ?>
  </div>
  <div class="locations-container full-width">
    <div class="max-width">
    <h2>Europe</h2>
    <?php while ( have_rows('europe_locations') ) { the_row(); ?>

      <div class="one-fifth">
        <?php if ( get_sub_field('location_name') ) { ?><h3><?php the_sub_field('location_name'); ?></h3><?php } ?>
        <?php if ( get_sub_field('location_address') ) { ?><address><?php the_sub_field('location_address'); ?></address> <?php } ?>
        <?php if ( get_sub_field('location_phone') ) { ?><a href="tel:<?php the_sub_field('location_phone'); ?>">TEL <?php the_sub_field('location_phone'); ?></a> <?php } ?>
        <?php if ( get_sub_field('location_fax') ) { ?><a>FAX <?php the_sub_field('location_fax'); ?></a><?php } ?>
      </div>

    <?php } ?>
    </div>
  </div>
</div>
<?php } else {
  // no rows found
} ?>

<?php if ( have_rows('asia_locations') ) { ?>
<div class="asia">
  <div class="location-map">
    <?php the_field('asia_iframe'); ?>
  </div>
  <div class="locations-container full-width">
    <div class="max-width">
    <h2>Asia</h2>
    <?php while ( have_rows('asia_locations') ) { the_row(); ?>

      <div class="one-fifth">
        <?php if ( get_sub_field('location_name') ) { ?><h3><?php the_sub_field('location_name'); ?></h3><?php } ?>
        <?php if ( get_sub_field('location_address') ) { ?><address><?php the_sub_field('location_address'); ?></address> <?php } ?>
        <?php if ( get_sub_field('location_phone') ) { ?><a href="tel:<?php the_sub_field('location_phone'); ?>">TEL <?php the_sub_field('location_phone'); ?></a> <?php } ?>
        <?php if ( get_sub_field('location_fax') ) { ?><a>FAX <?php the_sub_field('location_fax'); ?></a><?php } ?>
      </div>

    <?php } ?>
    </div>
  </div>
</div>
<?php } else {
  // no rows found
} ?>