<?php /*
DISPLAY PAGE CONTENT IN CARD FORM
*/ ?>

<?php if( have_rows('cards') ) { 
  $category='';
  $cat_class='';
  ?>
  <div class="page-cards-container max-width">
		<a id="3" class="anchor"></a>

    <?php while ( have_rows('cards') ) { the_row(); 
      $category=get_sub_field('card_title');
      ?>
    <?php if(is_page('careers')){ ?>
      <div class='page-card one-half'>
  <?php } elseif (is_page('capabilities')) { 
    if($category==='Injection Molding'){
      $category='injection-molding active';
      $cat_class='injection selected';
    } elseif($category==='Plastic Decorating') {
      $category='plastic-decorating';
      $cat_class='plastic';
    } elseif($category==='Electronics Assembly'){
      $category='electronic-components';
      $cat_class='electronic'; 
    } elseif($category==='Value-Added Assembly'){
      $category='value-added-assembly';
      $cat_class='value-added';
    } elseif ($category==='Textile Processing') {
      $category='textile-processing';
      $cat_class='textile';
    }

    ?>
      <div class="page-card one-fourth <?php echo $category ?>">
      <?php if ( is_page('20') ) { //capabilities ?>
        <a href="#2" class="smoothScroll capabilities-link"></a>
      <?php } ?>
  <?php } else { ?>
      <div class="page-card one-third">
  <?php } ?>
        <?php if(get_sub_field('card_image')){
            $image = get_sub_field('card_image'); 
         } ?>
        <?php if(is_page('capabilities')){ ?>
        <div class="page-card-img">
          <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"/>
          <?php if(get_sub_field('card_title')){?><h2><?php the_sub_field('card_title'); ?></h2><?php } ?>
        </div>
        <?php } else { ?>
        <div class="page-card-img" style="background-image: url('<?php echo $image['url']; ?>');">
          <?php if(get_sub_field('card_title')){?><h2><?php the_sub_field('card_title'); ?></h2><?php } ?>
        </div>
        <?php } ?>
        <?php if(get_sub_field('card_title')){?>
        <div class="page-card-content <?php echo $cat_class ?>">
          <?php the_sub_field('card_description'); ?>
        </div>

        <?php } ?>
      </div>

    <?php } ?>
  </div>
<?php } ?>