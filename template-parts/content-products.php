<?php /*
DISPLAY PRODUCT CATEGORIES | DISPLAY PRODUCTS | DISPLAY PRODUCT DETAILS
*/ ?>



<?php
  if (isset($_GET['product'])) {
    $passed_active_product = $_GET['product'];
  }
  if (isset($_GET['category'])) {
    $passed_active_category = $_GET['category'];
  }
?>






<div class='full-width featured-products'>
  <a id="10" class="anchor"></a>
  <ul class="max-width product-categories">
    <?php if ( $passed_active_category == 'carpet' ) { ?>
      <li class="electronics"><img src="../wp-content/themes/AGM/img/electronics-icon.png" id="electronics-icon"/>Electronics &amp; Lighting</li>
      <li class="carpeting active"><img src="../wp-content/themes/AGM/img/carpeting-icon.png" id="carpeting-icon"/><img src="../wp-content/themes/AGM/img/durmont-logo-no-back.png" id="durmont-logo"/?>Textiles</li>
    <?php } else { ?>
        <li class="electronics active"><img src="../wp-content/themes/AGM/img/electronics-icon.png" id="electronics-icon"/>Electronics &amp; Lighting</li>
        <li class="carpeting"><img src="../wp-content/themes/AGM/img/carpeting-icon.png" id="carpeting-icon"/><img src="../wp-content/themes/AGM/img/durmont-logo-no-back.png" id="durmont-logo"/?>Textiles</li>
    <?php } ?>
  </ul>

  <?php if ( $passed_active_category == 'carpet' ) { ?>

    <ul class="max-width electronics-products">
      <li class="door-lighting">Door Lighting</li>
      <li class="overhead-systems">Overhead Systems</li>
      <li class="trunk-cargo">Trunk and Cargo Lighting</li>
      <li class="instrument-panels">Instrument Panels and Floor Consoles</li>
      <li class="exterior-lighting">Exterior Lighting</li>
    </ul>
    <ul class="product-categories-mobile electronics-products">
      <li class="door-lighting active">Door Lighting</li>
      <li class="overhead-systems">Overhead Systems</li>
      <li class="trunk-cargo">Trunk and Cargo Lighting</li>
      <li class="instrument-panels">Instrument Panels and Floor Consoles</li>
      <li class="exterior-lighting">Exterior Lighting</li>
    </ul>

  <?php } else { ?>


    <ul class="max-width electronics-products selected">
      <?php if ( $passed_active_category == 'door' || is_null($passed_active_category) ) { ?>
        <li class="door-lighting active">Door Lighting</li>
      <?php } else { ?>
        <li class="door-lighting">Door Lighting</li>
      <?php } ?>
      <?php if ( $passed_active_category == 'overhead' ) { ?>
        <li class="overhead-systems active">Overhead Systems</li>
      <?php } else { ?>
        <li class="overhead-systems">Overhead Systems</li>
      <?php } ?>
      <li class="trunk-cargo">Trunk and Cargo Lighting</li>
      <?php if ( $passed_active_category == 'instrument' ) { ?>
        <li class="instrument-panels active">Instrument Panels and Floor Consoles</li>
      <?php } else { ?>
        <li class="instrument-panels">Instrument Panels and Floor Consoles</li>
      <?php } ?>
      <li class="exterior-lighting">Exterior Lighting</li>
    </ul>
    <ul class="product-categories-mobile electronics-products">
      <?php if ( $passed_active_category == 'door' || is_null($passed_active_category) ) { ?>
        <li class="door-lighting active">Door Lighting</li>
      <?php } else { ?>
        <li class="door-lighting">Door Lighting</li>
      <?php } ?>
      <?php if ( $passed_active_category == 'overhead' ) { ?>
        <li class="overhead-systems active">Overhead Systems</li>
      <?php } else { ?>
        <li class="overhead-systems">Overhead Systems</li>
      <?php } ?>
      <li class="trunk-cargo">Trunk and Cargo Lighting</li>
      <?php if ( $passed_active_category == 'instrument' ) { ?>
        <li class="instrument-panels active">Instrument Panels and Floor Consoles</li>
      <?php } else { ?>
        <li class="instrument-panels">Instrument Panels and Floor Consoles</li>
      <?php } ?>
      <li class="exterior-lighting">Exterior Lighting</li>
    </ul>


  <?php } ?>




  <ul class="max-width carpeting-products"> 
  </ul>
  <ul class="product-categories-mobile carpeting-products">
  </ul>

<div class="full-width products-display clearfix">
  <div class="max-width">

    <?php if ( $passed_active_category == 'door' || is_null($passed_active_category)) { ?>
      <h2 class="door-lighting active">Door Lighting</h2>
      <p class="door-lighting active">Products include: Map Pocket Lights, Lightpipes, Door Handles, Decorative Trim, and Puddle Lamps</p>
    <?php } else { ?>
      <h2 class="door-lighting">Door Lighting</h2>
      <p class="door-lighting">Products include: Map Pocket Lights, Lightpipes, Door Handles, Decorative Trim, and Puddle Lamps</p>
    <?php } ?>
    <?php if ( $passed_active_category == 'overhead' ) { ?>
      <h2 class="overhead-systems active">Overhead Systems</h2>
      <p class="overhead-systems active">Products include: Overhead Consoles, Dome and Rail Lamps, Overhead Cargo Lighting, Assist/Grab Handles, Switches</p>
    <?php } else { ?>
      <h2 class="overhead-systems">Overhead Systems</h2>
      <p class="overhead-systems">Products include: Overhead Consoles, Dome and Rail Lamps, Overhead Cargo Lighting, Assist/Grab Handles, Switches</p>
    <?php } ?>
    <?php if ( $passed_active_category == 'carpet' ) { ?>
      <h2 class="carpeting active">Textiles</h2>
      <p class="carpeting active">Products include: Floor Mats, Trunk and Cargo Mats, and Carpet Rolls

</p>
    <?php } else { ?>
      <h2 class="carpeting">Textiles</h2>
      <p class="carpeting">Products include: Floor Mats, Trunk and Cargo Mats, and Carpet Rolls

</p>
    <?php } ?>
    <?php if ( $passed_active_category == 'instrument' ) { ?>
      <h2 class="instrument-panels active">Instrument Panels and Floor Consoles</h2>
      <p class="instrument-panels active">Products include: Shift Indicators and Mechanisms, Light Pipes, Footwell Lighting, and Compartment Lighting</p>
    <?php } else { ?>
      <h2 class="instrument-panels">Instrument Panels and Floor Consoles</h2>
      <p class="instrument-panels">Products include: Shift Indicators and Mechanisms, Light Pipes, Footwell Lighting, and Compartment Lighting</p>
    <?php } ?>
    <h2 class="trunk-cargo">Trunk and Cargo Lighting</h2>
    <p class="trunk-cargo">Products include: Trunk and Cargo Lighting</p>
    <h2 class="exterior-lighting">Exterior Lighting</h2>
    <p class="exterior-lighting">Products include: Puddle Lamps and other AGM Exterior Lighting</p>




  <?php  //PULL ALL PRODUCTS
    $args = array(
      'post_type'   => 'products',
      'posts_per_page' => '-1'
    );
    $the_query = new WP_Query( $args );
  ?>
  <?php if( $the_query->have_posts() ) { ?>
    <?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
      <?php 
      $num = get_the_id();
      // SHOW YOAST PRIMARY CATEGORY, OR FIRST CATEGORY
        $category = get_the_category();
        $useCatLink = false;
        $cat_class='';
      // If post has a category assigned.
        if ($category){
          $category_display = '';
          $category_link = '';
          $category_description ='';
          if ( class_exists('WPSEO_Primary_Term') )
            {
          // Show the post's 'Primary' category, if this Yoast feature is available, & one is set
            $wpseo_primary_term = new WPSEO_Primary_Term( 'category', get_the_id() );
            $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
            $term = get_term( $wpseo_primary_term );
            if (is_wp_error($term)) { 
           // Default to first category (not Yoast) if an error is returned
              $category_display = $category[0]->name;
              $category_description = $category[0]->description;
              $category_link = get_category_link( $category[0]->term_id );
            } else { 
          // Yoast Primary category
              $category_display = $term->name;
              $category_description = $term->description;
              $category_link = get_category_link( $term->term_id );
            }
          } 
          else {
        // Default, display the first category in WP's list of assigned categories
            $category_display = $category[0]->name;
            $category_description = $category[0]->description;
            $category_link = get_category_link( $category[0]->term_id );
          }
            if($category_display==='Door Lighting') {
              if ( $passed_active_category == 'door' || is_null($passed_active_category)) { 
                $cat_class='door selected';
              } else {
                $cat_class='door';
              }
            } elseif ($category_display==='Overhead Systems') {
              if ( $passed_active_category == 'overhead' ) {
                $cat_class='overhead selected';
              } else {
                $cat_class='overhead';
              }
            } elseif ($category_display==='Textiles') {
              if ( $passed_active_category == 'carpet' ) {
                $cat_class='carpet selected';
              } else {
                $cat_class='carpet';
              }
            } elseif ($category_display==='Instrument Panels and Floor Consoles') {
              if ( $passed_active_category == 'instrument' ) {
                $cat_class='instrument selected';
              } else {
                $cat_class='instrument';
              }
            } elseif ($category_display==='Exterior Lighting'){
              $cat_class='exterior';
            } elseif($category_display==='Trunk and Cargo Lighting'){
              $cat_class='trunk';
            } else {
              $cat_class='product';
            }
        } ?>  
      <div class="single-product-preview <?php echo $cat_class; ?>">
      <?php 
        if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it. ?>
          <img src='<?php the_post_thumbnail_url(); ?>'/>
        <?php } ?>  

     <?php 
      // Display category
        if ( !empty($category_display) ){ ?>
          <p><?php echo $category_display; ?></p>
        <?php } else {
            // nothing
        }?>
        <h1><?php the_title(); ?></h1>
        <a href="#content" class="smoothScroll green-button" id="<?php echo $num; ?>">LEARN MORE</a>
      </div>

    <?php endwhile; ?>
  <?php } ?>
  <?php wp_reset_query();  // Restore global post data stomped by the_post(). ?>
    </div>
  </div>


<div class="product-info-container locations-container products-content-display full-width">
<a id='content' id="content" class="anchor"></a>

  <div class="max-width">
    <?php  //PULL ALL PRODUCTS
    $args = array(
      'post_type'   => 'products',
      'posts_per_page' => '-1'
    );
    $the_query = new WP_Query( $args );
  ?>
  <?php if( $the_query->have_posts() ) { ?>
    <?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
      <?php $num= get_the_id(); ?>
      <?php 
      // SHOW YOAST PRIMARY CATEGORY, OR FIRST CATEGORY
        $category = get_the_category();
        $useCatLink = false;
        $cat_class='';
      // If post has a category assigned.
        if ($category){
          $category_display = '';
          $category_link = '';
          $category_description ='';
          if ( class_exists('WPSEO_Primary_Term') )
            {
          // Show the post's 'Primary' category, if this Yoast feature is available, & one is set
            $wpseo_primary_term = new WPSEO_Primary_Term( 'category', get_the_id() );
            $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
            $term = get_term( $wpseo_primary_term );
            if (is_wp_error($term)) { 
           // Default to first category (not Yoast) if an error is returned
              $category_display = $category[0]->name;
              $category_description = $category[0]->description;
              $category_link = get_category_link( $category[0]->term_id );
            } else { 
          // Yoast Primary category
              $category_display = $term->name;
              $category_description = $term->description;
              $category_link = get_category_link( $term->term_id );
            }
          } 
          else {
        // Default, display the first category in WP's list of assigned categories
            $category_display = $category[0]->name;
            $category_description = $category[0]->description;
            $category_link = get_category_link( $category[0]->term_id );
          }
        } ?>  
      <div class="single-product-content full-width <?php echo $num; ?> clearfix
        <?php if ( $passed_active_product  == $num ) { ?>
            selected
        <?php } ?>
      ">
      <?php 
        if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it. ?>
          <img class='one-half' src='<?php the_post_thumbnail_url(); ?>'/>
        <?php } ?>  

     <?php ?>
      <div class="one-half">
      <a href="#10" id="close-product" class="smoothScroll" style='text-align: right;
    font-size: 1.25em;
    cursor: pointer;'><span>CLOSE</span></a>
       <?php if ( !empty($category_display) ){
             //Display category
            ?>
          <p><?php echo $category_display; ?></p>
        <?php } else {
            // nothing
        }?>
        <h1><?php the_title(); ?></h1>
        <p><?php the_content(); ?></p>
      </div>
    </div>
    <?php endwhile; ?>
  <?php } ?>    
  <?php wp_reset_query();  // Restore global post data stomped by the_post(). ?>

  </div>
</div>
</div>