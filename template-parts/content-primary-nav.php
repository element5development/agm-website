<?php /*
PRIMARY NAVIGATION
*/ ?>

<?php if (  is_page_template( 'template-styleguide.php' ) ) { ?>
  <header id="sticky-nav" class="main-nav full-width">
  <div class='primary-nav'>
    <div class="max-width clearfix">
      <a href="/"><img id="nav-logo" src="/wp-content/uploads/2016/10/AGM_FLEX-color-Logo-primary.png" alt="AGM Automotive" /></a>
      <a href="about/#3"><img id="sticky-nav-logo" src="<?php bloginfo('stylesheet_directory'); ?>/img/main-nav-sticky-logo.png" alt="AGM Automotive Originativity" /></a>
      <h2>Styleguide</h2>
    </div>
    </div>
  </header>
<?php } else { ?>
  <header id="sticky-nav" class="main-nav full-width">
    <div class="utility-nav">
      <div class="max-width">
			<!-- <a href="about/#3"><img id="nav-logo" src="<?php bloginfo('stylesheet_directory'); ?>/img/originativity-logo.png" alt="AGM Automotive Originativity" /></a>
        <span class="phone-link"><img id="phone-image" src="<?php bloginfo('stylesheet_directory'); ?>/img/phone-dark.svg" alt="AGM Automotive Phone"><a href="tel: <?php the_field('contact_phone', 'options'); ?>"><?php the_field('contact_phone', 'options'); ?></a></span>
        <div class="social-links">
          <?php if ( get_field('facebook', 'option') ) { ?><a href="https://www.facebook.com/agminc" target="_blank" class="facebook social-link"></a><?php } ?>
          <?php if ( get_field('instagram', 'option') ) { ?><a href="/styleguide" target="_blank" class="instagram social-link"></a><?php } ?>
          <?php if ( get_field('twitter', 'option') ) { ?><a href="https://twitter.com/AGM_HQ" target="_blank" class="twitter social-link"></a><?php } ?>
          <?php if ( get_field('pinterest', 'option') ) { ?><a href="/styleguide" target="_blank" class="pinterest social-link"></a><?php } ?>
          <?php if ( get_field('linkedin', 'option') ) { ?><a href="https://www.linkedin.com/company-beta/151067?pathWildcard=151067" target="_blank" class="linkedin social-link"></a><?php } ?>
          <?php if ( get_field('youtube', 'option') ) { ?><a href="/styleguide" target="_blank" class="youtube social-link"></a><?php } ?>
        </div> -->
        <div class="search">
          <span class="search-link"><img id="search-image" src="<?php bloginfo('stylesheet_directory'); ?>/img/search-dark.svg" alt="search"></span>
        </div>
				<!-- <div class="country-select">
          <a href="#" class="country"><img id="flag-image" src="<?php bloginfo('stylesheet_directory'); ?>/img/flag-us.png" alt='united states flag'>ENG</a>
        </div> -->
      </div>
    </div>
    <!--SEARCH BAR APPEAR-->
    <div class="search-bar">
      <form role="search" method="get" class="search-form" action="https://e5agm.wpengine.com/">
        <label>
          <span class="screen-reader-text">Search for:</span>
          <input type="search" class="search-field" placeholder="Search &hellip;" value="" name="s" />
        </label>
        <button type="submit" class="search-submit"><span class="screen-reader-text">Search</span></button>
      </form>
      <div id="close-bar"><img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-close.png" alt="Close Search Bar"></div>
    </div>
    <div class="primary-nav">
      <div class="max-width">
        <a href="/"><img id="nav-logo" src="<?php bloginfo('stylesheet_directory'); ?>/img/main-nav-logo.png" alt="AGM Automotive Originativity" /></a>
        <a href="/"><img id="sticky-nav-logo" src="<?php bloginfo('stylesheet_directory'); ?>/img/main-nav-sticky-logo.png" alt="AGM Automotive Originativity" /></a>
        <nav>
          <?php wp_nav_menu( array( 'theme_location' => 'primary-menu' ) ); ?>
        </nav> 
        <div style="clear: both"></div>
      </div>
    </div>
  </header>
  <header id="sticky-nav" class="mobile-nav full-width">
    <div class="utility-nav">
      <div class="max-width">
        <!-- <a href="/"><img id="nav-logo" src="<?php bloginfo('stylesheet_directory'); ?>/img/originativity-logo.png" alt="AGM Automotive Originativity" /></a>
        <div class="social-links">
          <?php if ( get_field('facebook', 'option') ) { ?><a href="/styleguide" class="facebook social-link"></a><?php } ?>
          <?php if ( get_field('instagram', 'option') ) { ?><a href="/styleguide" class="instagram social-link"></a><?php } ?>
          <?php if ( get_field('twitter', 'option') ) { ?><a href="/styleguide" class="twitter social-link"></a><?php } ?>
          <?php if ( get_field('pinterest', 'option') ) { ?><a href="/styleguide" class="pinterest social-link"></a><?php } ?>
          <?php if ( get_field('linkedin', 'option') ) { ?><a href="/styleguide" class="linkedin social-link"></a><?php } ?>
          <?php if ( get_field('youtube', 'option') ) { ?><a href="/styleguide" class="youtube social-link"></a><?php } ?>
        </div> -->
        <div class="search">
          <span class="search-link"><img id="search-image" src="<?php bloginfo('stylesheet_directory'); ?>/img/search-dark.svg" alt="search"></span>
        </div>
        <!-- <div class="country-select">
          <a href="#" class="country"><img id="flag-image" src="<?php bloginfo('stylesheet_directory'); ?>/img/flag-us.png" alt='united states flag'>ENG</a>
        </div> -->
      </div>
    </div>
        <!--SEARCH BAR APPEAR-->
    <div class="search-bar">
      <form role="search" method="get" class="search-form" action="https://e5agm.wpengine.com/">
        <label>
          <span class="screen-reader-text">Search for:</span>
          <input type="search" class="search-field" placeholder="Search &hellip;" value="" name="s" />
        </label>
        <button type="submit" class="search-submit"><span class="screen-reader-text">Search</span></button>
      </form>
      <div id="close-bar"><img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-close.png" alt="Close search bar"></div>
    </div>
    <div class="primary-nav">
      <div class="max-width">
        <span class="mobile-phone-link"><a href="tel: <?php the_field('contact_phone', 'options'); ?>"><img id="phone-image" src="<?php bloginfo('stylesheet_directory'); ?>/img/phone-dark.svg" alt="AGM Automotive Phone"></br>CALL</a></span>
        <span class="mobile-phone-link-stuck"><a href="tel: <?php the_field('contact_phone', 'options'); ?>"><img id="phone-image" src="<?php bloginfo('stylesheet_directory'); ?>/img/phone-dark.svg" alt="AGM Automotive Phone"></a></span>
        <a href="/"><img id="nav-logo" src="<?php bloginfo('stylesheet_directory'); ?>/img/main-nav-logo.png" alt="AGM Automotive Originativity" /></a>
        <a href="/"><img id="sticky-nav-logo" src="<?php bloginfo('stylesheet_directory'); ?>/img/main-nav-sticky-logo.png" alt="AGM Automotive Originativity" /></a>
        <span class="mobile-menu-link"><img id="menu-icon" src="<?php bloginfo('stylesheet_directory'); ?>/img/mobile-menu-icon.png" alt="AGM Automotive Phone"></br>MENU</a></span>
        <span class="mobile-menu-link-stuck"><img id="phone-image" src="<?php bloginfo('stylesheet_directory'); ?>/img/mobile-menu-icon.png" alt="AGM Automotive Phone"></a></span>
          <nav>
            <img id="close-nav" src="<?php bloginfo('stylesheet_directory');?>/img/rectangle-2.png" alt="Close navigation">
            <?php wp_nav_menu( array( 'theme_location' => 'primary-menu' ) ); ?>
          </nav> 
        <div style="clear: both"></div>
      </div>
    </div>
  </header>
<?php } ?>