<?php /*
SLICK SLIDER OF AWARDS FROM QUALITY PAGE
*/ ?>
<?php if(is_page('about')){ ?> 
  <div class="timeline-slider">
  <?php if( have_rows('awards') ) {
      while ( have_rows('awards') ) : the_row(); ?>
        <div class="award-slide">
          <div class="award-content">
        	 <h4><?php the_sub_field('award_year'); ?></h4>
        	 <p><?php the_sub_field('awards_list'); ?></p>
          </div> 
        </div>
      <?php endwhile;
  } else {
      //NOTHING
  } ?>
</div>
<?php } elseif (is_page('quality')) { ?>
  <div class="awards-slider">
  <?php if( have_rows('awards') ) {
      while ( have_rows('awards') ) : the_row(); ?>
        <div class="award-slide">
          <div class="award-content">
           <h4><?php the_sub_field('award_year'); ?></h4>
           <p><?php the_sub_field('awards_list'); ?></p>
          </div> 
        </div>
      <?php endwhile;
  } else {
      //NOTHING
  } ?>
</div><?php }?>