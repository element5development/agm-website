<?php /*
BLOG POST PREVIEW AS SHOWN ON THE NEWS/BLOG PAGE
*/ ?>

  <div class="certificate one-half">
     <?php $thumbnail = get_the_post_thumbnail_url(); 
      $alt_text = get_post_meta($post->ID, '_wp_attachment_image_alt', true);
     ?>
      <div class="cert-image">
        <img src="<?php echo $thumbnail; ?>" alt="<?php echo $atl_text; ?>">
      </div>
      <div class="cert-content">
        <p class='post-date'><?php echo get_the_date();?></p>
        <h3 class='post-title'><?php the_title(); ?></h3>
        <p class='post-excerpt'><?php echo get_the_excerpt(); ?></p>
        <a class='green-button' style="padding:15px 35px 15px 35px;" href="<?php echo get_permalink(); ?>">Read the Rest</a>
      </div>
  </div>
