<?php /*
THE TEMPLATE FOR DISPLAYING 404 PAGES (BROKEN LINKS)
*/ ?>

<?php get_header(); ?>

<main class="full-width">
	<section class="not-found">
      <div id="error-option-404" class="error-option">
        <h1>404</h1>
        <h2>Sorry, we didn't find what you're looking for.</h2>
        <p>You can either try using our main navigation above or try a key word search using the form below.</p>
        <div class="search-bar">
        	      <form role="search" method="get" class="search-form" action="https://e5agm.wpengine.com/">
        <label>
          <span class="screen-reader-text">Search for:</span>
          <input type="search" class="search-field" placeholder="Search &hellip;" value="" name="s" />
        </label>
        <button type="submit" class="search-submit"><span class="screen-reader-text">Search</span></button>
      </form>
      	</div>
      </div>
	</section>
</main>

<?php get_footer(); ?>
