<?php /*
Template Name: Styleguide
*/ ?>

<?php get_header(); ?>

<main class="full-width">
  <div class="styleguide-container max-width clearfix">
    <div id="styleguide-sidebar" class="sidebar">
      <nav>
        <ul class="menu">
          <li><a class="smoothScroll" href="#styleguide-logo">Logos</a></li>
          <li><a class="smoothScroll" href="#styleguide-colors">Colors</a></li>
          <li><a class="smoothScroll" href="#styleguide-typography">Typography</a></li>
          <li><a class="smoothScroll" href="#styleguide-buttons">Buttons</a></li>
          <li><a class="smoothScroll" href="#styleguide-forms">Forms</a></li>
          <?php if ( get_field('main_navigation') ) { ?><li><a class="smoothScroll" href="#styleguide-navigation">Navigation</a></li><?php } ?>
          <?php if( have_rows('icons') ) { ?><li><a class="smoothScroll" href="#styleguide-icons">Icons</a></li><?php } ?>
          <?php if( have_rows('patterns') ) { ?><li><a class="smoothScroll" href="#styleguide-patterns">Imagery</a></li><?php } ?>
        </ul>
      </nav> 
    </div>
    <div id="styleguide-contents" class="sidebar-page-contents">

      <section class="logo-section">
        <a id="styleguide-logo" class="styleguide-anchor"></a>
        <h2>Logo</h2>
        <nav class="sub-section-nav">
          <ul>
            <li><a href="#primary-logo" class="smoothScroll">Primary</a></li>
            <li><a href="#variant-logos" class="smoothScroll">Variants</a></li>
          </ul>
        </nav>
        <div class="primary-logo styleguide-sub-section clearfix full-width"> 
          <a id="primary-logo" class="styleguide-anchor"></a> 
          <h3>Primary Logo</h3>
            <div class="full-width">
              <a target="_blank" href="<?php the_field('primary_logo'); ?>"><img src="<?php the_field('primary_logo'); ?>" /></a>
            </div>
        </div>
        <?php if( have_rows('logo_variant') ) { $i = 0; ?>
          <div class="variant-logos styleguide-sub-section clearfix full-width"> 
            <a id="variant-logos" class="styleguide-anchor"></a> 
            <h3>Variants</h3>
            <p class="legal-text">*Click to download any logo</p>
            <?php while ( have_rows('logo_variant') ) : the_row(); $i++; ?>
              <div class="one-half">
                <div class="varient-image vertical-align-parent" style="background-color: <?php the_sub_field('background_color'); ?>">
                  <div class="vertical-align-content">
                    <a target="_blank" href="<?php the_sub_field('logo'); ?>"><img src="<?php the_sub_field('logo'); ?>" /></a>
                  </div>
                </div>
                <div class="varient-description">
                  <p class="legal-text"><?php the_sub_field('description'); ?></p>
                </div>
              </div>
              <?php if ( $i % 2 == 0 ) { ?>
                <div style="clear: both"></div>
              <?php } ?>
            <?php endwhile; ?>
          </div>
        <?php } else {
          //NOTHING
        } ?>
      </section>

      <section class="styleguide-section">
        <a id="styleguide-colors" class="styleguide-anchor"></a>
        <h2>Colors</h2>
        <nav class="sub-section-nav">
          <ul>
            <li><a href="#primary-colors" class="smoothScroll">Primary</a></li>
            <li><a href="#secondary-colors" class="smoothScroll">Secondary</a></li>
          </ul>
        </nav>

        <?php if( have_rows('primary_colors') ) { ?>
          <div class="primary-colors styleguide-sub-section clearfix full-width"> 
            <a id="primary-colors" class="styleguide-anchor"></a> 
            <h3>Primary Colors</h3>
            <?php while ( have_rows('primary_colors') ) : the_row(); ?>
              <div class="one-fourth">
                <div class="color-swatch" style="background-color: <?php the_sub_field('color_hex_code'); ?>;"></div>
                <div class="color-swatch-details">
                  <?php
                    $rgb = hex2rgb( get_sub_field('color_hex_code') );
                    $cmyk = rgb2cmyk(hex2rgb2( get_sub_field('color_hex_code') ));
                  ?>
                  <p><b><?php the_sub_field('color_name'); ?></b></p>
                  <p class="legal-text"><?php the_sub_field('color_hex_code'); ?></p>
                  <p class="legal-text"><?php echo $rgb; ?></p>
                  <p class="legal-text"><?php echo $cmyk; ?></p>
                  <p class="legal-text"><?php the_sub_field('color_pantone_code'); ?></p>
                </div>
              </div>
            <?php endwhile; ?>
          </div>
        <?php } else {
          //NOTHING
        } ?>

        <?php if( have_rows('secondary_colors') ) { ?>
          <div class="secondary-colors styleguide-sub-section clearfix full-width"> 
            <a id="secondary-colors" class="styleguide-anchor"></a> 
            <h3>Secondary Colors</h3>
            <?php while ( have_rows('secondary_colors') ) : the_row(); ?>
              <div class="one-fourth">
                <div class="color-swatch" style="background-color: <?php the_sub_field('color_hex_code'); ?>;"></div>
                <div class="color-swatch-details">
                  <?php
                    $rgb = hex2rgb( get_sub_field('color_hex_code') );
                    $cmyk = rgb2cmyk(hex2rgb2( get_sub_field('color_hex_code') ));
                  ?>
                  <p><b><?php the_sub_field('color_name'); ?></b></p>
                  <p class="legal-text"><?php the_sub_field('color_hex_code'); ?></p>
                  <p class="legal-text"><?php echo $rgb; ?></p>
                  <p class="legal-text"><?php echo $cmyk; ?></p>
                  <p class="legal-text"><?php the_sub_field('color_pantone_code'); ?></p>
                </div>
              </div>
            <?php endwhile; ?>
          </div>
        <?php } else {
          //NOTHING
        } ?>
      </section>

      <section class="styleguide-section">
        <a id="styleguide-typography" class="styleguide-anchor"></a>
        <h2>Typography</h2>
        <nav class="sub-section-nav">
          <ul>
            <li><a href="#typeface" class="smoothScroll">Typeface</a></li>
            <li><a href="#headlines" class="smoothScroll">Headlines</a></li>
            <li><a href="#paragraph" class="smoothScroll">Paragraph</a></li>
            <li><a href="#text-formatting" class="smoothScroll">Text Formatting</a></li>
          </ul>
        </nav>

        <?php if( have_rows('typeface') ) { ?>
          <div class="typeface styleguide-sub-section clearfix full-width"> 
            <a id="typeface" class="styleguide-anchor"></a> 
            <h3>Typeface</h3>
            <?php while ( have_rows('typeface') ) : the_row(); ?>
              <div class="one-fourth vertical-align-parent">
                <div class="typeface-option vertical-align-content">
                  <p><span style="
                  <?php
                    //FONT FAMILY
                    if ( get_sub_field('font_fammily') == 'Header' ) {
                      echo "font-family: 'Titillium Web', sans-serif; ";
                    } elseif ( get_sub_field('font_fammily') == 'Body' ) {
                      echo "font-family: 'Muli', sans-serif; ";
                    } else {
                      echo "font-family: 'Muli', sans-serif; "; //no third font
                    }
                    //ITALIC?
                    if ( get_sub_field('font_italic') == 'Yes' ) {
                      echo "font-style: italic; ";
                    } else {
                      echo "font-style: none; ";
                    }
                    //FONT WEIGHT
                    echo "font-weight: ";
                    the_sub_field('font_weight');
                    echo ";";
                  ?>
                  ">Aa Zz</span></p>
                  <p class="legal-text"><?php the_sub_field('font_name'); ?></p>
                </div>
              </div>
            <?php endwhile; ?>
          </div>
        <?php } else {
          //NOTHING
        } ?>

        <div class="heaadlines styleguide-sub-section clearfix full-width"> 
          <a id="headlines" class="styleguide-anchor"></a> 
          <h3>Headlines</h3>
          <div class="headline-option">
            <h1>Heading One</h1>
            <p class="legal-text">
              <?php 
                $base = get_field('base_font_size', 'option'); 
                $font = $base * get_field('h1_font_size', 'option');
                $lh = ( $font * get_field('h1_line_height', 'option') ) / 100;
                $rgb = hex2rgb( get_field('h1_color', 'option') );
                $cmyk = rgb2cmyk(hex2rgb2( get_field('h1_color', 'option') ));
              ?>
              <?php the_field('h1_font_size', 'option'); ?>em/<?php the_field('h1_line_height', 'option'); ?>% or <?php echo $font; ?>px/<?php echo $lh; ?>px
              | Font Weight: <?php the_field('h1_font_weight', 'option'); ?> 
              | Char Spacing: <?php the_field('h1_character_spacing', 'option'); ?>px 
              | <?php the_field('h1_color', 'option'); ?> or <?php echo $rgb; ?> or <?php echo $cmyk; ?>
            </p>
          </div>
          <div class="headline-option">
            <h2>Heading Two</h2>
            <p class="legal-text">
              <?php 
                $base = get_field('base_font_size', 'option'); 
                $font = $base * get_field('h2_font_size', 'option');
                $lh = ( $font * get_field('h2_line_height', 'option') ) / 100;
                $rgb = hex2rgb( get_field('h2_color', 'option') );
                $cmyk = rgb2cmyk(hex2rgb2( get_field('h2_color', 'option') ));
              ?>
              <?php the_field('h2_font_size', 'option'); ?>em/<?php the_field('h2_line_height', 'option'); ?>% or <?php echo $font; ?>px/<?php echo $lh; ?>px
              | Font Weight: <?php the_field('h2_font_weight', 'option'); ?> 
              | Char Spacing: <?php the_field('h2_character_spacing', 'option'); ?>px 
              | <?php the_field('h2_color', 'option'); ?> or <?php echo $rgb; ?> or <?php echo $cmyk; ?>
            </p>
          </div>
          <div class="headline-option">
            <h3>Heading Three</h3>
            <p class="legal-text">
              <?php 
                $base = get_field('base_font_size', 'option'); 
                $font = $base * get_field('h3_font_size', 'option');
                $lh = ( $font * get_field('h3_line_height', 'option') ) / 100;
                $rgb = hex2rgb( get_field('h3_color', 'option') );
                $cmyk = rgb2cmyk(hex2rgb2( get_field('h3_color', 'option') ));
              ?>
              <?php the_field('h3_font_size', 'option'); ?>em/<?php the_field('h3_line_height', 'option'); ?>% or <?php echo $font; ?>px/<?php echo $lh; ?>px
              | Font Weight: <?php the_field('h3_font_weight', 'option'); ?> 
              | Char Spacing: <?php the_field('h3_character_spacing', 'option'); ?>px 
              | <?php the_field('h3_color', 'option'); ?> or <?php echo $rgb; ?> or <?php echo $cmyk; ?>
            </p>
          </div>
          <div class="headline-option">
            <h4>Heading Four</h4>
            <p class="legal-text">
              <?php 
                $base = get_field('base_font_size', 'option'); 
                $font = $base * get_field('h4_font_size', 'option');
                $lh = ( $font * get_field('h4_line_height', 'option') ) / 100;
                $rgb = hex2rgb( get_field('h4_color', 'option') );
                $cmyk = rgb2cmyk(hex2rgb2( get_field('h4_color', 'option') ));
              ?>
              <?php the_field('h4_font_size', 'option'); ?>em/<?php the_field('h4_line_height', 'option'); ?>% or <?php echo $font; ?>px/<?php echo $lh; ?>px
              | Font Weight: <?php the_field('h4_font_weight', 'option'); ?> 
              | Char Spacing: <?php the_field('h4_character_spacing', 'option'); ?>px 
              | <?php the_field('h4_color', 'option'); ?> or <?php echo $rgb; ?> or <?php echo $cmyk; ?>
            </p>
          </div>
          <div class="headline-option">
            <h5>Heading Five</h5>
            <p class="legal-text">
              <?php 
                $base = get_field('base_font_size', 'option'); 
                $font = $base * get_field('h5_font_size', 'option');
                $lh = ( $font * get_field('h5_line_height', 'option') ) / 100;
                $rgb = hex2rgb( get_field('h5_color', 'option') );
                $cmyk = rgb2cmyk(hex2rgb2( get_field('h5_color', 'option') ));
              ?>
              <?php the_field('h5_font_size', 'option'); ?>em/<?php the_field('h5_line_height', 'option'); ?>% or <?php echo $font; ?>px/<?php echo $lh; ?>px
              | Font Weight: <?php the_field('h5_font_weight', 'option'); ?> 
              | Char Spacing: <?php the_field('h5_character_spacing', 'option'); ?>px 
              | <?php the_field('h5_color', 'option'); ?> or <?php echo $rgb; ?> or <?php echo $cmyk; ?>
            </p>
          </div>
          <div class="headline-option">
            <h6>Heading Six</h6>
            <p class="legal-text">
              <?php 
                $base = get_field('base_font_size', 'option'); 
                $font = $base * get_field('h6_font_size', 'option');
                $lh = ( $font * get_field('h6_line_height', 'option') ) / 100;
                $rgb = hex2rgb( get_field('h6_color', 'option') );
                $cmyk = rgb2cmyk(hex2rgb2( get_field('h6_color', 'option') ));
              ?>
              <?php the_field('h6_font_size', 'option'); ?>em/<?php the_field('h6_line_height', 'option'); ?>% or <?php echo $font; ?>px/<?php echo $lh; ?>px
              | Font Weight: <?php the_field('h6_font_weight', 'option'); ?> 
              | Char Spacing: <?php the_field('h6_character_spacing', 'option'); ?>px 
              | <?php the_field('h6_color', 'option'); ?> or <?php echo $rgb; ?> or <?php echo $cmyk; ?>
            </p>
          </div>
          <div class="headline-option">
            <p>Body Content</p>
            <p class="legal-text">
              <?php 
                $base = get_field('base_font_size', 'option'); 
                $font = $base * get_field('p_font_size', 'option');
                $lh = ( $font * get_field('p_line_height', 'option') ) / 100;
                $rgb = hex2rgb( get_field('p_color', 'option') );
                $cmyk = rgb2cmyk(hex2rgb2( get_field('p_color', 'option') ));
              ?>
              <?php the_field('p_font_size', 'option'); ?>em/<?php the_field('p_line_height', 'option'); ?>% or <?php echo $font; ?>px/<?php echo $lh; ?>px
              | Font Weight: <?php the_field('p_font_weight', 'option'); ?> 
              | Char Spacing: <?php the_field('p_character_spacing', 'option'); ?>px 
              | <?php the_field('p_color', 'option'); ?> or <?php echo $rgb; ?> or <?php echo $cmyk; ?>
            </p>
          </div>
          <div class="headline-option">
            <p class="block-quote">Block Quote</p>
            <p class="legal-text">
              <?php 
                $base = get_field('base_font_size', 'option'); 
                $font = $base * get_field('bq_font_size', 'option');
                $lh = ( $font * get_field('bq_line_height', 'option') ) / 100;
                $rgb = hex2rgb( get_field('bq_color', 'option') );
                $cmyk = rgb2cmyk(hex2rgb2( get_field('bq_color', 'option') ));
              ?>
              <?php the_field('bq_font_size', 'option'); ?>em/<?php the_field('bq_line_height', 'option'); ?>% or <?php echo $font; ?>px/<?php echo $lh; ?>px
              | Font Weight: <?php the_field('bq_font_weight', 'option'); ?> 
              | Char Spacing: <?php the_field('bq_character_spacing', 'option'); ?>px 
              | <?php the_field('bq_color', 'option'); ?> or <?php echo $rgb; ?> or <?php echo $cmyk; ?>
            </p>
          </div>          
        </div>

        <div class="paragraph styleguide-sub-section clearfix full-width"> 
          <a id="paragraph" class="styleguide-anchor"></a> 
          <h3>Paragraph</h3>
          <h3>Headline</h3>
          <p>Cras mattis consectetur purus sit amet fermentum. Donec sed odio dui. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Etiam porta sem malesuada magna mollis euismod. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</p>
          <h4>Subheadline</h4>
          <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Curabitur blandit tempus porttitor. Etiam porta sem malesuada magna mollis euismod. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Aenean lacinia bibendum nulla sed.</p>
          <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh,</p>
        </div>

        <div class="paragraph styleguide-sub-section clearfix full-width"> 
          <a id="text-formatting" class="styleguide-anchor"></a> 
          <h3>Text Formatting</h3>
          <?php the_field('text_formatting'); ?>
        </div>
      </section>

      <section class="styleguide-section">
        <a id="styleguide-buttons" class="styleguide-anchor"></a>
        <h2>Buttons</h2>
        <nav class="sub-section-nav">
          <ul>
            <li><a href="#p-buttons" class="smoothScroll">Primary</a></li>
            <li><a href="#s-buttons" class="smoothScroll">Secondary</a></li>
            <li><a href="#social-icons" class="smoothScroll">Social</a></li>
          </ul>
        </nav>

         <div class="p-buttons styleguide-sub-section clearfix full-width"> 
          <a id="p-buttons" class="styleguide-anchor"></a> 
          <h3>Primary</h3>
          <div class="button-continer">
            <a href="/styleguide" class="green-button">Primary Button</a>
          </div>
          <div class="dark-bg button-continer">
            <a href="/styleguide" class="green-button">Primary Button</a>
          </div>
        </div>

        <div class="s-buttons styleguide-sub-section clearfix full-width"> 
          <a id="s-buttons" class="styleguide-anchor"></a> 
          <h3>Secondary</h3>
          <div class="button-continer">
            <a href="/styleguide" class="glow" style="margin-top: 0;margin-bottom: 0;">Secondary Button</a>
          </div>
          <div class="dark-bg button-continer">
            <a href="/styleguide" class="glow" style="margin-top: 0;margin-bottom: 0;">Secondary Button</a>
          </div>
        </div>

        <div class="paragraph styleguide-sub-section clearfix full-width"> 
          <a id="social-icons" class="styleguide-anchor"></a> 
          <h3>Social Icons</h3>
          <div class="social-links">
            <?php if ( get_field('facebook', 'option') ) { ?><a href="/styleguide" class="facebook social-link" style="background-size:auto 25px; height: 50px; width: 50px; background-color: transparent;"></a><?php } ?>
            <?php if ( get_field('instagram', 'option') ) { ?><a href="/styleguide" class="instagram social-link" style="background-size:auto 25px; height: 50px; width: 50px; background-color: transparent;"></a><?php } ?>
            <?php if ( get_field('twitter', 'option') ) { ?><a href="/styleguide" class="twitter social-link" style="background-size:auto 25px; height: 50px; width: 50px; background-color: transparent;"></a><?php } ?>
            <?php if ( get_field('pinterest', 'option') ) { ?><a href="/styleguide" class="pinterest social-link" style="background-size:auto 25px; height: 50px; width: 50px; background-color: transparent;"></a><?php } ?>
            <?php if ( get_field('linkedin', 'option') ) { ?><a href="/styleguide" class="linkedin social-link" style="background-size:auto 25px; height: 50px; width: 50px; background-color: transparent;"></a><?php } ?>
            <?php if ( get_field('youtube', 'option') ) { ?><a href="/styleguide" class="youtube social-link" style="background-size:auto 25px; height: 50px; width: 50px; background-color: transparent;"></a><?php } ?>
          </div>
        </div>

      </section>
      <?php if ( get_field('main_navigation') ) { ?>
        <section class="styleguide-section">
          <a id="styleguide-navigation" class="styleguide-anchor"></a>
          <h2>Navigation</h2>
          <nav class="sub-section-nav">
            <ul>
              <li><a href="#desktop" class="smoothScroll">Desktop</a></li>
              <li><a href="#mobile" class="smoothScroll">Mobile</a></li>
            </ul>
          </nav>
          <?php if ( get_field('main_navigation') ) { ?>
            <div class="desktop styleguide-sub-section clearfix full-width"> 
              <a id="desktop" class="styleguide-anchor"></a> 
              <h3>Desktop</h3>
              <?php if ( get_field('main_navigation') ) { ?>
                <h4>Main Navigation</h4>
                <img src="<?php the_field('main_navigation'); ?>" />
              <?php } ?>
              <?php if ( get_field('sticky_navigation') ) { ?>
                <h4>Sticky Navigation</h4>
                <img src="<?php the_field('sticky_navigation'); ?>" />
              <?php } ?>
              <?php if ( get_field('main_footer_navn') ) { ?>
                <h4>Footer Navigation</h4>
                <img src="<?php the_field('footer_nav'); ?>" />
              <?php } ?>
            </div>
          <?php } ?>
          <?php if ( get_field('mobile_navigation') ) { ?>
            <div class="mobile styleguide-sub-section clearfix full-width"> 
              <a id="mobile" class="styleguide-anchor"></a> 
              <h3>Mobile</h3>
              <?php if ( get_field('mobile_navigation') ) { ?>
                <h4>Main Navigation</h4>
                <img src="<?php the_field('mobile_navigation'); ?>" />
              <?php } ?>
              <?php if ( get_field('mobile_navigation_open') ) { ?>
                <h4>Main Navigation Open</h4>
                <img src="<?php the_field('mobile_navigation_open'); ?>" />
              <?php } ?>
              <?php if ( get_field('mobile_footer_navigation') ) { ?>
                <h4>Footer Navigation</h4>
                <img src="<?php the_field('mobile_footer_navigation'); ?>" />
              <?php } ?>
            </div>
          <?php } ?>
        </section>
      <?php } ?>

      <section class="styleguide-section">
        <a id="styleguide-forms" class="styleguide-anchor"></a>
        <h2>Forms</h2>
        <?php echo do_shortcode('[gravityform id="2" title="false" description="false"]'); ?>
      </section>

      <?php if( have_rows('icons') ) { ?>
        <section class="styleguide-section">
          <a id="styleguide-icons" class="styleguide-anchor"></a>
          <h2>Icons</h2>
          <div class="icons-container clearfix">
                <?php while ( have_rows('icons') ) : the_row(); ?>
                  <div class="one-fourth">
                    <div class="icon-container">
                     <a target="_blank" href="<?php the_sub_field('icon'); ?>"><img src="<?php the_sub_field('icon'); ?>" /></a>
                    </div>
                  </div>
                <?php endwhile; ?>
          </div>
          <a href="<?php the_field('icon_zip_download'); ?>" class="primary-button">Download All</a>
          <p class="legal-text">*Click to download any icon</p>
        </section>
      <?php } ?>

      <?php if( have_rows('patterns') ) { ?>
        <section class="styleguide-section">
          <a id="styleguide-patterns" class="styleguide-anchor"></a>
          <h2>Imagery</h2>
          <div class="patterns-container clearfix">
                <?php while ( have_rows('patterns') ) : the_row(); ?>
                  <div class="full-width">
                    <div class="pattern-container">
                      <a target="_blank" href="<?php the_sub_field('pattern'); ?>"><img src="<?php the_sub_field('pattern'); ?>" /></a>
                    </div>
                  </div>
                <?php endwhile; ?>
          </div>
          <a href="<?php the_field('pattern_zip_download'); ?>" class="primary-button">Download All</a>
          <p class="legal-text">*Click to download any pattern</p>
        </section>
      <?php } ?>
      
    </div>
  </div>
</main>

<?php get_footer(); ?>