/* DECLARATIONS FOR ANIMATIONS, STICKY PORTIONS, SLIDERS, ETC. | ONLY MINIFIED VERSIONS ARE CALLED IN THEME */
/*ENTRANCE ANIMATIONS*/
jQuery(document).ready(function() {
    jQuery(".classorid").addClass("hidden").viewportChecker({
        classToAdd: "visible animated fadeIn",
        offset: 100
    });
    $('.navigation-container li:first-of-type').addClass('active');
});
/* STICKY PORTIONS*/
jQuery(document).ready(function() {
    var window_width = jQuery(window).width();
    if (window_width < 750) {
        //DISABLE STICKY PARTS ON MOBILE
        // jQuery("#sticky-nav").trigger("sticky_kit:detach");
        // jQuery("#styleguide-sidebar").trigger("sticky_kit:detach");
    } else {
        //ENABLE STICKY PARTS
        jQuery("#sticky-nav").stick_in_parent({
            parent: "body",
            offset_top: -48,
        });
        jQuery("#styleguide-sidebar").stick_in_parent({
            offset_top: 125
        });
    }
    jQuery(window).resize(function() {
        window_width = jQuery(window).width();
        if (window_width < 750) {
            //DISABLE STICKY PARTS ON MOBILE
            // jQuery("#sticky-nav").trigger("sticky_kit:detach");
            // jQuery("#styleguide-sidebar").trigger("sticky_kit:detach");
        } else {
            //ENABLE STICKY PARTS
            jQuery("#sticky-nav").stick_in_parent({
                parent: "body"
            });
            jQuery("#styleguide-sidebar").stick_in_parent({
                offset_top: 125
            });
        }
    });
});
/*SLICK SLIDERS*/
$('.logo-slider .max-width').slick({
    autoplay: true,
    autoplaySpeed: 3000,
    arrows: true,
    dots: false,
    infinite: false,
    speed: 300,
    slidesToShow: 5,
    slidesToScroll: 1,
    centerMode: true,
    infinite: true,
    responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 4,
      }
    },
    {
      breakpoint: 800,
      settings: {
        slidesToShow: 3,
      }
    },
    {
      breakpoint: 500,
      settings: {
        slidesToShow: 1,
      }
    }
  ]
});
$('.awards-slider').slick({
    autoplay: true,
    autoplaySpeed: 3000,
    arrows: true,
    dots: false,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 1,
    infinite: true,
    responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 2,
      }
    },
    {
      breakpoint: 801,
      settings: {
        slidesToShow: 1,
      }
    },
    {
      breakpoint: 501,
      settings: {
        slidesToShow: 1,
      }
    }
  ]
});
$('.timeline-slider').slick({
    autoplay: true,
    autoplaySpeed: 3000,
    arrows: true,
    dots: false,
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 1,
    infinite: true,
    centerMode: true,
    centerPadding: '250px',
    responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 1,
        centerPadding: '200px',
      }
    },
    {
      breakpoint: 801,
      settings: {
        slidesToShow: 1,
        centerPadding: '100px',
      }
    },
    {
      breakpoint: 501,
      settings: {
        slidesToShow: 1,
        centerPadding: '25px',
      }
    }
  ]
});
$('.locations-slider').slick({
    autoplay: true,
    autoplaySpeed: 5000,
    arrows: true,
    dots: false,
    infinite: false,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    centerMode: true,
    infinite: true,
    responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 1,
      }
    },
    {
      breakpoint: 800,
      settings: {
        slidesToShow: 1,
      }
    },
    {
      breakpoint: 500,
      settings: {
        slidesToShow: 1,
      }
    }
  ]
});
$('.bio-slider').slick({
  dots: false,
  arrows: true,
  asNavFor: '.bios',
});
$('.bios').slick({
  slidesToShow: 15,
  slidesToScroll: 1,
  asNavFor: '.bio-slider',
  dots: false,
  centerMode: true,
  focusOnSelect: true
});


var stHeight = $('.bio-slider .slick-track').height();
$('.bio-slider .slick-slide').css('height',stHeight + 'px' );
$('.leadership-preview').on('click',function(){
    $('.bio-overlay-slider').addClass('open');
});
$('.single-bio .overlay-close').on('click',function(){
    $('.bio-overlay-slider').removeClass('open');
});

//SEARCH BAR OPEN AND CLOSE
$('.search').on('click',function(){
    $('.search-bar').addClass('show');
});
$('#close-bar img').on('click',function(){
    $('.search-bar').removeClass('show');
});

/* Mobile Navigation Open and Close */
$('.mobile-menu-link').on('click',function(){
    $('.mobile-nav .primary-nav nav').addClass('show');
});
$('#close-nav').on('click',function(){
    $('.mobile-nav .primary-nav nav').removeClass('show');
});


/*map iframe disable scrol zoom till clicked*/
$('.north-america').on('click',function(){
    $(this).children('.location-map').addClass('clicked');
});
$('.asia').on('click',function(){
    $(this).children('.location-map').addClass('clicked');
});
$('.europe').on('click',function(){
    $(this).children('.location-map').addClass('clicked');
});

//DISPLAY FEATURED PRODUCTS BY CATEGORY && DISPLAY CATEGORIES ON PRODUCT PAGE 
$('.product-categories li').click(function() {
    $('.featured-products li').removeClass('active');
    $('.featured-products ul').removeClass('selected');
    $('.products-display h2').removeClass('active');
    $('.products-display p').removeClass('active');
    $('.single-product-preview').removeClass('selected');
    $('.page-id-1617.locations-container.products-content-display').removeClass('active');
    $('.single-product-content').removeClass('selected');
    var category = $(this).attr('class');
    $(this).addClass('active');
    console.log(category);

    if(category === "door-lighting") {
        $('.single-product-preview.door').addClass('selected');
    }
    else if(category==="electronics"){
        $('ul.max-width.electronics-products').addClass('selected');
        $('.door-lighting').addClass('active');
        $('.door').addClass('selected');
    }
    else if(category==="carpeting"){
        $('ul.carpeting-products').addClass('selected');
        $('.carpeting').addClass('active');
        $('.carpet').addClass('selected');
    }
    else if(category === "overhead-systems") {
        $('.overhead').addClass('selected');
    } else if(category === "instrument-panels") {
        $('.instrument').addClass('selected');
    } else {
    };
});
$('.electronics-products li').click(function() {
    $('.electronics-products li').removeClass('active');
    $('.products-display h2').removeClass('active');
    $('.products-display p').removeClass('active');
    $('.single-product-preview').removeClass('selected');
    $('.page-id-1617 .locations-container.products-content-display').removeClass('active');
    $('.single-product-content').removeClass('selected');
    var category = $(this).attr('class');
    $(this).addClass('active');
    console.log(category);

    if(category === "door-lighting") {
        $('.door-lighting').addClass('active');
        $('.door').addClass('selected');
    }
    else if(category === "overhead-systems") {
        $('.overhead-systems').addClass('active');
        $('.overhead').addClass('selected');
    } else if(category === "instrument-panels") {
        $('.instrument-panels').addClass('active');
        $('.instrument').addClass('selected');
    } else if(category === "trunk-cargo") {
        $('.trunk-cargo').addClass('active');
        $('.trunk').addClass('selected');
    } else if(category === "exterior-lighting") {
        $('.exterior-lighting').addClass('active');
        $('.exterior').addClass('selected');
    };
});
$('.product-categories-mobile li.active').click(function() {
    $('.product-categories-mobile ul li').addClass('show');
  // $('.single-product-preview').removeClass('selected');
  // $('.featured-products li').removeClass('active');
  // $('.featured-products ul').removeClass('selected');
  // $('.product-categories-mobile li').removeClass('show');
  // $('.product-categories-mobile li').removeClass('active');
  // var category = $(this).attr('class');
  // console.log(category);
  // $('.product-categories-mobile li').addClass('show');
  // $(this).addClass('active');
  // if(category=='door-lighting'){
  //   $('.product-categories-mobile li').removeClass('show');
  //   $('.product-categories-mobile li').removeClass('active');
  //   $('.product-categories-selected li.door-lighting').addClass('active');
  //   $('.door-lighting').addClass('active');
  //   $('.door').addClass('selected');
  // } else if(category=='overhead-systems'){ 
  //   $('.overhead-systems').addClass('active');
  //   $('.overhead').addClass('selected');
  // }else if(category=='carpeting') {
  //       $('.carpeting').addClass('active');
  //       $('.carpet').addClass('selected');
  // } else if(category=='instrument-panels') {
  //       $('.instrument-panels').addClass('active');
  //       $('.instrument').addClass('selected');
  // }
});
//DISPLAY PRODUCT CONTENT SLIDER 
$(".single-product-preview a.green-button").click(function(){
    $('.page-id-1617 .locations-container.products-content-display').removeClass('active');
    $('.single-product-content').removeClass('selected');
    var number = $(this).attr('id');
    $('.page-id-1617 .locations-container.products-content-display').addClass('active');
    $('.'+number).addClass('selected');
});
$(".page-id-4 a[href$='#content']").click(function(){
    var number = $(this).attr('id');
    console.log(number);
  $(function() {
      $('.page-id-1617 .locations-container.products-content-display').addClass('active');
      $('.'+number).addClass('selected');
    });
});
$('a#close-product').on('click', function(e){
  $('.page-id-1617 .locations-container.products-content-display').removeClass('active');
  $('.single-product-content').removeClass('selected');
});
//DISPLAY LOCATION INFORMATION BY CONTINENT
$('.anchor-navigation li a').click(function(){
    var category =$(this).attr('href');
    $('.anchor-navigation li').removeClass('active');
    $(this).parent('li').addClass('active');

    if(category==="#1"){
        $('.europe').removeClass('selected');
        $('.asia').removeClass('selected');
        $('.north-america').addClass('selected');
        $('.location-map').removeClass('clicked');
    } else if (category==="#2") {
        $('.north-america').removeClass('selected');
        $('.asia').removeClass('selected');
        $('.europe').addClass('selected');
        $('.location-map').removeClass('clicked');
    } else if (category==="#3"){
        $('.north-america').removeClass('selected');
        $('.europe').removeClass('selected');
        $('.asia').addClass('selected');
        $('.location-map').removeClass('clicked');
    }
})

//DISPLAY CAPABILITIES
$('.page-id-20 .benefits li').click(function() {
    var category = $(this).attr('class');
    $('.page-id-20 .benefits li').removeClass('active');
    $(".page-card").removeClass("selected");
    $(this).addClass('active');
        if(category==='single-benefit injmold'){
        $(".page-card").removeClass("active");
        $(".injection-molding").addClass("active");
        $(".page-card-content").removeClass("selected");
        $(".injection").addClass("selected");
    } else if(category==='single-benefit plasdec') {
        $(".page-card").removeClass("active");
        $(".plastic-decorating").addClass("active");
        $(".page-card-content").removeClass("selected");
        $(".plastic").addClass("selected");      
    } else if(category==='single-benefit electcomp'){
        $(".page-card").removeClass("active");
        $(".electronic-components").addClass("active");
        $(".page-card-content").removeClass("selected");
        $(".electronic").addClass("selected");  
    } else if(category==='single-benefit valueaa'){
        $(".page-card").removeClass("active");
        $(".value-added-assembly").addClass("active");
        $(".page-card-content").removeClass("selected");
        $(".value-added").addClass("selected");
    } else if(category==='single-benefit textproc') {
        $(".page-card").removeClass("active");
        $(".textile-processing").addClass("active");
        $(".page-card-content").removeClass("selected");
        $(".textile").addClass("selected");
    }
});
$(".page-card-content img").click(function(){
    $(".card-expand").removeClass('active');
    var category = $(this).attr('class');
    $('.page-id-20 .benefits li').removeClass('active');
    $(".page-card").removeClass('active');
    $(".page-card-content").removeClass("selected");
    if(category==="card-expand injection-molding"){
        $(".page-card.injection-molding").addClass("active");
        $(".page-card-content.injection").addClass("selected");
        $('.single-benefit.injmold').addClass("active");
    } else if(category==="card-expand plastic-decorating"){
        $(".page-card.plastic-decorating").addClass("active");
        $(".page-card-content.plastic").addClass("selected");
        $('.single-benefit.plasdec').addClass("active");
    } else if(category==="card-expand electronic-components"){
        $(".page-card.electronic-components").addClass("active");
        $(".page-card-content.electronic").addClass("selected");
        $('.single-benefit.electcomp').addClass("active");
    } else if(category==="card-expand value-added-assembly"){
        $(".page-card.value-added-assembly").addClass("active");
        $(".page-card-content.value-added").addClass("selected");
        $('.single-benefit.valueaa').addClass("active");
    } else if(category==="card-expand textile-processing"){
        $(".page-card.textile-processing").addClass("active");
        $(".page-card-content.textile").addClass("selected");
        $('.single-benefit.textproc').addClass("active");
    }
});

/* FANCYBOX */
$("#gallery-2 .gallery-item .gallery-icon a").fancybox();

/* DISPLAY LEADERSHIP BIOS */
$(".certificate a.open-bio.green-button").click(function(){
    $('.certificate').removeClass('active');
    $(this).parent('div').parent('div').addClass('active');
});
$("a.close-bio").click(function(){
    $('.certificate').removeClass('active');
});

/* PLANETARY.JS */
var pathname = window.location.pathname;

if(pathname === '/about/') {

(function() {
  var globe = planetaryjs.planet();
  // Load our custom `autorotate` plugin; see below.
  globe.loadPlugin(autorotate(10));
  // The `earth` plugin draws the oceans and the land; it's actually
  // a combination of several separate built-in plugins.
  //
  // Note that we're loading a special TopoJSON file
  // (world-110m-withlakes.json) so we can render lakes.
  globe.loadPlugin(planetaryjs.plugins.earth({
    topojson: { file:   'http://agmautomotive.com/wp-content/themes/AGM/js/world-110m-withlakes.json' },
    oceans:   { fill:   'rgba(0, 12, 58, 0.33)' },
    land:     { fill:   '#0fc396' },
    borders:  { stroke: 'rgba(0,0,0,0)' }
  }));
  // Load our custom `lakes` plugin to draw lakes; see below.
  globe.loadPlugin(lakes({
    fill: 'rgba(0, 12, 58, 0.66)'
  }));
  // The `pings` plugin draws animated pings on the globe.
  globe.loadPlugin(planetaryjs.plugins.pings());
  // The `zoom` and `drag` plugins enable
  // manipulating the globe with the mouse.
  globe.loadPlugin(planetaryjs.plugins.zoom({
    scaleExtent: [100, 300]
  }));
  globe.loadPlugin(planetaryjs.plugins.drag({
    // Dragging the globe should pause the
    // automatic rotation until we release the mouse.
    onDragStart: function() {
      this.plugins.autorotate.pause();
    },
    onDragEnd: function() {
      this.plugins.autorotate.resume();
    }
  }));
  // Set up the globe's initial scale, offset, and rotation.
  globe.projection.scale(175).translate([175, 175]).rotate([0, -10, 0]);

  // Every few hundred milliseconds, we'll draw another random ping.
  setInterval(function() {
    var color = '#def9a5';
    globe.plugins.pings.add(-83.1648201, 42.5507245, { color: color, ttl: 2000, angle: 10 });//Global Headquarters
    globe.plugins.pings.add(-83.0504677, 42.5679354, { color: color, ttl: 2000, angle: 10 }); //Progress Drive
    globe.plugins.pings.add(-83.1666997, 42.550125, { color: color, ttl: 2000, angle: 10 }); //Northwood Operations
    globe.plugins.pings.add(-100.171176, 20.6235145, { color: color, ttl: 2000, angle: 10 }); //Mexico
    globe.plugins.pings.add(-84.1880071, 10.0004295, { color: color, ttl: 2000, angle: 10 }); //Costa Rica
    globe.plugins.pings.add(15.979317, 47.2765816, { color: color, ttl: 2000, angle: 10 }); //Europe
    globe.plugins.pings.add(120.9550013, 31.301049, { color: color, ttl: 2000, angle: 10 }); //Asian HQ
    globe.plugins.pings.add(121.0338787, 31.5634511, { color: color, ttl: 2000, angle: 10 }); //Taicang China
    globe.plugins.pings.add(16.9040453, 47.2479579, { color: color, ttl: 2000, angle: 10 }); //Sarvar
    globe.plugins.pings.add(9.2287642, 48.6458284, { color: color, ttl: 2000, angle: 10 }); //Stuttgart
    globe.plugins.pings.add(16.8452405, 46.817732, { color: color, ttl: 2000, angle: 10 }); //Zalaegerszeg
    globe.plugins.pings.add(-121.8976249, 37.4200549, { color: color, ttl: 2000, angle: 10 }); //Milpitas
  }, 150);

  var canvas = document.getElementById('globe');
  // Special code to handle high-density displays (e.g. retina, some phones)
  // In the future, Planetary.js will handle this by itself (or via a plugin).
  if (window.devicePixelRatio == 2) {
    canvas.width = 800;
    canvas.height = 800;
    context = canvas.getContext('2d');
    context.scale(2, 2);
  }
  // Draw that globe!
  globe.draw(canvas);

  // This plugin will automatically rotate the globe around its vertical
  // axis a configured number of degrees every second.
  function autorotate(degPerSec) {
    // Planetary.js plugins are functions that take a `planet` instance
    // as an argument...
    return function(planet) {
      var lastTick = null;
      var paused = false;
      planet.plugins.autorotate = {
        pause:  function() { paused = true;  },
        resume: function() { paused = false; }
      };
      // ...and configure hooks into certain pieces of its lifecycle.
      planet.onDraw(function() {
        if (paused || !lastTick) {
          lastTick = new Date();
        } else {
          var now = new Date();
          var delta = now - lastTick;
          // This plugin uses the built-in projection (provided by D3)
          // to rotate the globe each time we draw it.
          var rotation = planet.projection.rotate();
          rotation[0] += degPerSec * delta / 1000;
          if (rotation[0] >= 180) rotation[0] -= 360;
          planet.projection.rotate(rotation);
          lastTick = now;
        }
      });
    };
  };

  // This plugin takes lake data from the special
  // TopoJSON we're loading and draws them on the map.
  function lakes(options) {
    options = options || {};
    var lakes = null;

    return function(planet) {
      planet.onInit(function() {
        // We can access the data loaded from the TopoJSON plugin
        // on its namespace on `planet.plugins`. We're loading a custom
        // TopoJSON file with an object called "ne_110m_lakes".
        var world = planet.plugins.topojson.world;
        lakes = topojson.feature(world, world.objects.ne_110m_lakes);
      });

      planet.onDraw(function() {
        planet.withSavedContext(function(context) {
          context.beginPath();
          planet.path.context(context)(lakes);
          context.fillStyle = options.fill || 'black';
          context.fill();
        });
      });
    };
  };
})();
};
